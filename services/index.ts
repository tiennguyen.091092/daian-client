/** Generate by swagger-axios-codegen */
// @ts-nocheck
/* eslint-disable */

import { IRequestOptions, IRequestConfig, getConfigs, axios } from './serviceOptions';
export const basePath = '';

export interface IList<T> extends Array<T> {}
export interface List<T> extends Array<T> {}
export interface IDictionary<TValue> {
  [key: string]: TValue;
}
export interface Dictionary<TValue> extends IDictionary<TValue> {}

export interface IListResult<T> {
  items?: T[];
}

export class ListResultDto<T> implements IListResult<T> {
  items?: T[];
}

export interface IPagedResult<T> extends IListResult<T> {
  totalCount?: number;
  items?: T[];
}

export class PagedResultDto<T = any> implements IPagedResult<T> {
  totalCount?: number;
  items?: T[];
}

// customer definition
// empty

export class UsersService {
  /**
   * Thêm Người dùng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: UserEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Người dùng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyUserEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Người dùng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Người dùng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateUserDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Người dùng
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/users/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class AuthService {
  /**
   * Đăng nhập bằng SĐT/Password
   */
  static authControllerLogin(
    params: {
      /** requestBody */
      body?: LoginDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/login';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Đăng ký tài khoản
   */
  static authControllerRegister(
    params: {
      /** requestBody */
      body?: RegisterDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<UserEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/auth/register';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class PermissionsService {
  /**
   * Chi tiết Permissions
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PermissionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Permissions
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: PermissionEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PermissionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Permissions
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Permissions
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyPermissionEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Permissions
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: PermissionEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<PermissionEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/permissions';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RolesService {
  /**
   * Danh sách Role
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyRoleEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Role
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: RoleEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RoleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách chức năng
   */
  static roleControllerRoutes(options: IRequestOptions = {}): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/routes';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Cập nhật nhiều nhóm Quyền
   */
  static roleControllerUpdateMany(
    params: {
      /** requestBody */
      body?: RoleEntity[];
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/bulk';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Role
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RoleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Role
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: RoleEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RoleEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Role
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/roles/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CdnService {
  /**
   * Upload file User/Project
   */
  static cdnControllerUploadFile(
    params: {
      /**  */
      file: any;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/cdn/upload';

      const configs: IRequestConfig = getConfigs('post', 'multipart/form-data', url, options);

      let data = null;
      data = new FormData();
      if (params['file']) {
        if (Object.prototype.toString.call(params['file']) === '[object Array]') {
          for (const item of params['file']) {
            data.append('file', item as any);
          }
        } else {
          data.append('file', params['file'] as any);
        }
      }

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class BranchOfficeService {
  /**
   * Chi tiết Chi nhánh văn phòng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BranchOfficeEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/branch-office/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Chi nhánh văn phòng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: BranchOfficeEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BranchOfficeEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/branch-office/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Chi nhánh văn phòng
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/branch-office/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Chi nhánh văn phòng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyBranchOfficeEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/branch-office';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Chi nhánh văn phòng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: BranchOfficeEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<BranchOfficeEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/branch-office';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class CustomerService {
  /**
   * Chi tiết Customer
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customer/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Customer
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: CustomerEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customer/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Customer
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customer/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Customer
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyCustomerEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customer';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Customer
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CustomerEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<CustomerEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/customer';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class LocationService {
  /**
   * Chi tiết Địa điểm
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<LocationEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/location/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Địa điểm
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: LocationEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<LocationEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/location/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Địa điểm
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/location/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Địa điểm
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyLocationEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/location';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Địa điểm
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: LocationEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<LocationEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/location';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class DriverCarService {
  /**
   * Chi tiết DriverRouteCar
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DriverCarEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/driver-car/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa DriverRouteCar
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: DriverCarEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DriverCarEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/driver-car/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá DriverRouteCar
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/driver-car/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách DriverRouteCar
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyDriverCarEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/driver-car';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm DriverRouteCar
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: DriverCarEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<DriverCarEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/driver-car';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class OrderService {
  /**
   * Danh sách Order
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyOrderEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Order
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: CreateOrderDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Order
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: UpdateOrderDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Chi tiết Order
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Order
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thay đổi trạng thái đơn hàng
   */
  static orderControllerChangeStatus(
    params: {
      /** ID của đơn hàng */
      id: number;
      /** requestBody */
      body?: ChangeStatusDto;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<ChangeStatusDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order/change-status/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xóa Id chuyến hàng trong bảng đơn hàng và update lại status đơn hàng
   */
  static orderControllerOrderRemoveOnOrder(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/order/remove-onOrder/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class OnOrderService {
  /**
   * Chi tiết Chuyến hàng
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OnOrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/on-order/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Chuyến hàng
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: OnOrderEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OnOrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/on-order/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Chuyến hàng
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/on-order/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Chuyến hàng
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyOnOrderEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/on-order';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Chuyến hàng
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: OnOrderEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<OnOrderEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/on-order';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export class RouteService {
  /**
   * Chi tiết Tuyến
   */
  static getOneBase(
    params: {
      /**  */
      id: number;
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/route/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = { fields: params['fields'], join: params['join'], cache: params['cache'] };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Sửa Tuyến
   */
  static updateOneBase(
    params: {
      /**  */
      id: number;
      /** requestBody */
      body?: RouteEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/route/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('patch', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Xoá Tuyến
   */
  static deleteOneBase(
    params: {
      /**  */
      id: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/route/{id}';
      url = url.replace('{id}', params['id'] + '');

      const configs: IRequestConfig = getConfigs('delete', 'application/json', url, options);

      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Danh sách Tuyến
   */
  static getManyBase(
    params: {
      /** Selects resource fields. <a href="https://github.com/nestjsx/crud/wiki/Requests#select" target="_blank">Docs</a> */
      fields?: any | null[];
      /** Adds search condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#search" target="_blank">Docs</a> */
      s?: string;
      /** Adds filter condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#filter" target="_blank">Docs</a> */
      filter?: any | null[];
      /** Adds OR condition. <a href="https://github.com/nestjsx/crud/wiki/Requests#or" target="_blank">Docs</a> */
      or?: any | null[];
      /** Adds sort by field. <a href="https://github.com/nestjsx/crud/wiki/Requests#sort" target="_blank">Docs</a> */
      sort?: any | null[];
      /** Adds relational resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#join" target="_blank">Docs</a> */
      join?: any | null[];
      /** Limit amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#limit" target="_blank">Docs</a> */
      limit?: number;
      /** Offset amount of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#offset" target="_blank">Docs</a> */
      offset?: number;
      /** Page portion of resources. <a href="https://github.com/nestjsx/crud/wiki/Requests#page" target="_blank">Docs</a> */
      page?: number;
      /** Reset cache (if was enabled). <a href="https://github.com/nestjsx/crud/wiki/Requests#cache" target="_blank">Docs</a> */
      cache?: number;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<GetManyRouteEntityResponseDto> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/route';

      const configs: IRequestConfig = getConfigs('get', 'application/json', url, options);
      configs.params = {
        fields: params['fields'],
        s: params['s'],
        filter: params['filter'],
        or: params['or'],
        sort: params['sort'],
        join: params['join'],
        limit: params['limit'],
        offset: params['offset'],
        page: params['page'],
        cache: params['cache']
      };
      let data = null;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
  /**
   * Thêm Tuyến
   */
  static createOneBase(
    params: {
      /** requestBody */
      body?: RouteEntity;
    } = {} as any,
    options: IRequestOptions = {}
  ): Promise<RouteEntity> {
    return new Promise((resolve, reject) => {
      let url = basePath + '/route';

      const configs: IRequestConfig = getConfigs('post', 'application/json', url, options);

      let data = params.body;

      configs.data = data;
      axios(configs, resolve, reject);
    });
  }
}

export interface GetManyUserEntityResponseDto {
  /**  */
  data: UserEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface UserEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Email */
  email?: string;

  /** Mật khẩu */
  password: string;

  /** Họ tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;

  /** Role ID */
  roleId?: number;

  /** Địa chỉ */
  address?: string;

  /** Ngày sinh */
  dob?: Date;

  /** Giới tính */
  gender?: EnumUserEntityGender;

  /** Trạng thái */
  status: EnumUserEntityStatus;

  /** Ảnh khách hàng */
  avatar?: string;
}

export interface UpdateUserDto {
  /** Email */
  email?: string;

  /** Mật khẩu */
  password: string;

  /** Họ tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;

  /** Địa chỉ */
  address?: string;

  /** Ngày sinh */
  dob?: Date;

  /** Giới tính */
  gender?: EnumUpdateUserDtoGender;
}

export interface LoginDto {
  /** Số điện thoại */
  tel: string;

  /** Mật khẩu */
  password: string;
}

export interface RegisterDto {
  /** Email */
  email?: string;

  /** Mật khẩu */
  password: string;

  /** Họ tên */
  fullName: string;

  /** Số điện thoại */
  tel: string;

  /** Địa chỉ */
  address?: string;
}

export interface GetManyPermissionEntityResponseDto {
  /**  */
  data: PermissionEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface PermissionEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Hành động */
  action: string;
}

export interface GetManyRoleEntityResponseDto {
  /**  */
  data: RoleEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface RoleEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Tên nhóm */
  name: string;

  /** Quyền */
  permissions: string[];
}

export interface GetManyBranchOfficeEntityResponseDto {
  /**  */
  data: BranchOfficeEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface BranchOfficeEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Tên chi nhánh */
  branchOfficeName?: string;

  /** Địa chỉ */
  branchOfficeAddress?: string;

  /** Số điện thoại */
  branchOfficePhone: string;

  /** Người đại diện */
  representative?: string;

  /** Số điện thoại người đại diện */
  representativePhone: string;
}

export interface GetManyCustomerEntityResponseDto {
  /**  */
  data: CustomerEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface CustomerEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Tên khách hàng */
  customerName: string;

  /** Loại khách hàng */
  customerType?: EnumCustomerEntityCustomerType;

  /** Giới tính */
  customerGender?: EnumCustomerEntityCustomerGender;

  /** Số điện thoại */
  customerPhone: string;

  /** Địa chỉ khách hàng */
  customerAddress?: string;
}

export interface GetManyLocationEntityResponseDto {
  /**  */
  data: LocationEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface LocationEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Tên điểm */
  locationName?: string;
}

export interface GetManyDriverCarEntityResponseDto {
  /**  */
  data: DriverCarEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface DriverCarEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Tên tài xế */
  driverName: string;

  /** Biển số xe */
  licensePlates?: string;

  /** Số điện thoại */
  driverPhone: string;

  /** Loại xe */
  carType?: EnumDriverCarEntityCarType;
}

export interface GetManyOrderEntityResponseDto {
  /**  */
  data: OrderEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyOnOrderEntityResponseDto {
  /**  */
  data: OnOrderEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface GetManyRouteEntityResponseDto {
  /**  */
  data: RouteEntity[];

  /**  */
  count: number;

  /**  */
  total: number;

  /**  */
  page: number;

  /**  */
  pageCount: number;
}

export interface RouteEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Tên tuyến đường */
  name?: string;
}

export interface OnOrderEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Route ID */
  routeId?: number;

  /** Tuyến đường */
  route?: CombinedRouteTypes;

  /** Driver Car ID */
  driverCarId?: number;

  /** Xe tài xế */
  driverCar?: CombinedDriverCarTypes;

  /** Danh sách đơn hàng */
  orders?: OrderEntity[];

  /** Ghi chú */
  note: string;

  /** Ngày */
  date?: Date;

  /** Chuyến (giờ) */
  time?: string;

  /** Người tạo */
  createdById?: number;

  /** Người sửa cuối */
  modifiedById?: number;
}

export interface OrderEntity {
  /** ID */
  id?: number;

  /** Thời gian tạo */
  createdAt?: Date;

  /** Thời gian cập nhật */
  updatedAt?: Date;

  /** Thời gian xoá */
  deletedAt?: Date;

  /** Người tạo */
  createdById?: number;

  /** Người tạo */
  createdBy?: CombinedCreatedByTypes;

  /** Người sửa cuối */
  modifiedById?: number;

  /** Người sửa */
  modifiedBy?: CombinedModifiedByTypes;

  /** Mã đơn hàng */
  orderCode: string;

  /** CustomerSend ID */
  customerSendId?: number;

  /** Khách hàng gửi */
  customerSend?: CombinedCustomerSendTypes;

  /** Customer Received ID */
  customerReceivedId?: number;

  /** Khách hàng gửi */
  customerReceived?: CombinedCustomerReceivedTypes;

  /** Tên hàng hóa */
  itemName: string;

  /** Số lượng hàng hóa */
  quantity: number;

  /** Trọng lượng */
  weight: number;

  /** Giảm giá */
  discount: number;

  /** Cước (VNĐ) */
  orderMoney: number;

  /** Đã thu (VNĐ) */
  collectedMoney: number;

  /** Thu hộ (VNĐ) */
  cashCollection: number;

  /** Chưa thu (VNĐ) */
  notCollectedMoney: number;

  /** COD */
  cod: number;

  /** Phí COD */
  codFee: number;

  /** Ghi chú */
  note?: string;

  /** Địa chỉ giao */
  address: string;

  /** Trạng thái */
  status: EnumOrderEntityStatus;

  /** Hình thức thanh toán */
  paymentType: EnumOrderEntityPaymentType;

  /** Location ID */
  locationId?: number;

  /** Điểm giao */
  location?: CombinedLocationTypes;

  /** VP gửi */
  branchOfficeSendId?: number;

  /** Văn phòng gửi */
  branchOfficeSend?: CombinedBranchOfficeSendTypes;

  /** VP nhận */
  branchOfficeReceiveId?: number;

  /** Văn phòng nhận */
  branchOfficeReceive?: CombinedBranchOfficeReceiveTypes;

  /** On Order ID */
  onOrderId?: number;

  /** Lên hàng */
  onOrder?: CombinedOnOrderTypes;
}

export interface CreateOrderDto {
  /** Người tạo */
  createdById?: number;

  /** Người sửa cuối */
  modifiedById?: number;

  /** Mã đơn hàng */
  orderCode: string;

  /** CustomerSend ID */
  customerSendId?: number;

  /** Customer Received ID */
  customerReceivedId?: number;

  /** Tên hàng hóa */
  itemName: string;

  /** Số lượng hàng hóa */
  quantity: number;

  /** Trọng lượng */
  weight: number;

  /** Giảm giá */
  discount: number;

  /** Cước (VNĐ) */
  orderMoney: number;

  /** Đã thu (VNĐ) */
  collectedMoney: number;

  /** Thu hộ (VNĐ) */
  cashCollection: number;

  /** Chưa thu (VNĐ) */
  notCollectedMoney: number;

  /** COD */
  cod: number;

  /** Phí COD */
  codFee: number;

  /** Ghi chú */
  note?: string;

  /** Địa chỉ giao */
  address: string;

  /** Hình thức thanh toán */
  paymentType: EnumCreateOrderDtoPaymentType;

  /** Location ID */
  locationId?: number;

  /** VP gửi */
  branchOfficeSendId?: number;

  /** VP nhận */
  branchOfficeReceiveId?: number;
}

export interface UpdateOrderDto {
  /** ID */
  id?: number;

  /** Trạng thái */
  status: EnumUpdateOrderDtoStatus;

  /** Người tạo */
  createdById?: number;

  /** Người sửa cuối */
  modifiedById?: number;

  /** Mã đơn hàng */
  orderCode: string;

  /** CustomerSend ID */
  customerSendId?: number;

  /** Customer Received ID */
  customerReceivedId?: number;

  /** Tên hàng hóa */
  itemName: string;

  /** Số lượng hàng hóa */
  quantity: number;

  /** Trọng lượng */
  weight: number;

  /** Giảm giá */
  discount: number;

  /** Cước (VNĐ) */
  orderMoney: number;

  /** Đã thu (VNĐ) */
  collectedMoney: number;

  /** Thu hộ (VNĐ) */
  cashCollection: number;

  /** Chưa thu (VNĐ) */
  notCollectedMoney: number;

  /** COD */
  cod: number;

  /** Phí COD */
  codFee: number;

  /** Ghi chú */
  note?: string;

  /** Địa chỉ giao */
  address: string;

  /** Hình thức thanh toán */
  paymentType: EnumUpdateOrderDtoPaymentType;

  /** Location ID */
  locationId?: number;

  /** VP gửi */
  branchOfficeSendId?: number;

  /** VP nhận */
  branchOfficeReceiveId?: number;
}

export interface ChangeStatusDto {
  /** Trạng thái */
  status: EnumChangeStatusDtoStatus;

  /** On Order ID */
  onOrderId?: number;
}
export enum EnumUserEntityGender {
  'male' = 'male',
  'female' = 'female',
  'other' = 'other'
}
export enum EnumUserEntityStatus {
  'active' = 'active',
  'inactive' = 'inactive',
  'banned' = 'banned',
  'verify' = 'verify'
}
export enum EnumUpdateUserDtoGender {
  'male' = 'male',
  'female' = 'female',
  'other' = 'other'
}
export enum EnumCustomerEntityCustomerType {
  'personal' = 'personal',
  'enterprise' = 'enterprise'
}
export enum EnumCustomerEntityCustomerGender {
  'male' = 'male',
  'female' = 'female',
  'other' = 'other'
}
export enum EnumDriverCarEntityCarType {
  'cargo' = 'cargo',
  'passenger' = 'passenger',
  'other' = 'other'
}
export type CombinedRouteTypes = RouteEntity;
export type CombinedDriverCarTypes = DriverCarEntity;
export type CombinedCreatedByTypes = UserEntity;
export type CombinedModifiedByTypes = UserEntity;
export type CombinedCustomerSendTypes = CustomerEntity;
export type CombinedCustomerReceivedTypes = CustomerEntity;
export enum EnumOrderEntityStatus {
  'created' = 'created',
  'on_order' = 'on_order',
  'off_order' = 'off_order'
}
export enum EnumOrderEntityPaymentType {
  'in_office' = 'in_office',
  'receive_payment' = 'receive_payment'
}
export type CombinedLocationTypes = LocationEntity;
export type CombinedBranchOfficeSendTypes = BranchOfficeEntity;
export type CombinedBranchOfficeReceiveTypes = BranchOfficeEntity;
export type CombinedOnOrderTypes = OnOrderEntity;
export enum EnumCreateOrderDtoPaymentType {
  'in_office' = 'in_office',
  'receive_payment' = 'receive_payment'
}
export enum EnumUpdateOrderDtoStatus {
  'created' = 'created',
  'on_order' = 'on_order',
  'off_order' = 'off_order'
}
export enum EnumUpdateOrderDtoPaymentType {
  'in_office' = 'in_office',
  'receive_payment' = 'receive_payment'
}
export enum EnumChangeStatusDtoStatus {
  'created' = 'created',
  'on_order' = 'on_order',
  'off_order' = 'off_order'
}
