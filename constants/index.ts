export const SITE_NAME = 'Đại An Logistics'
export const SITE_SLOGAN = 'Đại An Logistics'
export const SHIPPING_PARTNER = {
  ghtk: 'Giao hàng tiết kiệm',
  ghn: 'Giao hàng nhanh',
  viettelpost: 'Viettel Post',
}
export const ROLE_TYPE = {
  admin: 'Admin',
  merchant: 'Đối tác',
  partner: 'Cộng tác viên',
}
