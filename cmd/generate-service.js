const { codegen: generateService } = require("swagger-axios-codegen");

generateService({
  methodNameMode: "operationId",
   //remoteUrl: "http://127.0.0.1:4000/docs-json",
 remoteUrl: "https://newapi.vanchuyendaian.com/docs-json",
  outputDir: "./services",
  strictNullChecks: true,
  useCustomerRequestInstance: true,
  modelMode: "interface",
  extendDefinitionFile: "./services/customerDefinition.ts",
  extendGenericType: ["JsonResult"],
  sharedServiceOptions: true,
  useStaticMethod: true,
  openApi: "3.0.0"
}).then(() => {
  // eslint-disable-next-line no-console
  console.log("OK");
});
