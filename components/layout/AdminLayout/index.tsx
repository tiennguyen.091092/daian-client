import {
  LogoutOutlined,
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
} from '@ant-design/icons'
import { Col, Dropdown, Layout, Menu, Row, Space, Spin } from 'antd'
import Sidebar from 'components/layout/AdminLayout/Sidebar'
import { withAuth } from 'hooks'
import { destroyCookie, setCookie } from 'nookies'
import React from 'react'
import { useRecoilState, useRecoilValue } from 'recoil'
import { authState, loadingState, sidebarCollapsedState } from 'recoil/Atoms'

const { Header, Content, Footer } = Layout
const Index: React.FC = ({ children }) => {
  const user = useRecoilValue(authState)
  const isLoading = useRecoilValue(loadingState)
  const [sidebarCollapsed, setsidebarCollapsed] = useRecoilState(
      sidebarCollapsedState
  )
  const handleCollapse = () => {
    setCookie(null, 'isSidebarCollapsed', sidebarCollapsed ? '1' : '0')
    setsidebarCollapsed(!sidebarCollapsed)
  }
  return (
      <Spin spinning={isLoading} tip="Đang tải...">
        <Layout className={'antd-pro-layout'}>
          <Sidebar />
          <Layout
              style={{
                marginLeft: sidebarCollapsed ? 80 : 200,
                transition: 'all 0.2s',
              }}
          >
            <Header className="header">
              <Row
                  justify="space-between"
                  style={{
                    boxShadow: '0 1px 4px rgb(0 21 41 / 8%)',
                    padding: '0 28px',
                  }}
              >
                <Col>
                  {React.createElement(
                      sidebarCollapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
                      {
                        className: 'trigger',
                        onClick: handleCollapse,
                      }
                  )}
                </Col>
                <Col>
                  <Space direction="horizontal" size={[24, 12]} wrap>
                    <Dropdown
                        overlay={
                          <Menu>
                            <Menu.Item key="setting:2">
                              <a
                                  href={'#'}
                                  onClick={() => {
                                    destroyCookie(null, 'token', {
                                      path: '/',
                                    })
                                    window.location.href = '/auth/login'
                                  }}
                              >
                                <LogoutOutlined /> Đăng xuất
                              </a>
                            </Menu.Item>
                          </Menu>
                        }
                        placement="bottomCenter"
                    >
                    <span>
                      <UserOutlined /> {user?.fullName}
                    </span>
                    </Dropdown>
                  </Space>
                </Col>
              </Row>
            </Header>
            <Content>
              <div className="site-layout-background">{children}</div>
            </Content>
            <Footer style={{ textAlign: 'center' }}>Đại An Logistics ©2023</Footer>
          </Layout>
        </Layout>
      </Spin>
  )
}

export default withAuth(Index)
// export default Id
