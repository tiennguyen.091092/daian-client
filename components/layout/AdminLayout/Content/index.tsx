import React from 'react'
import Head from 'next/head'
import {Button, ButtonProps} from 'antd'
import {PageHeader} from '@ant-design/pro-layout'
import {SITE_NAME} from '@/constants'
import Link from 'common/Link'

interface ContentButtonProps extends ButtonProps {
  text?: string
  visible?: boolean
  icon?: JSX.Element
}

interface Props {
  title?: string
  onBack?: (e?: React.MouseEvent<HTMLDivElement>) => void
  subTitle?: string
  buttons?: ContentButtonProps[]
  endButtons?: ContentButtonProps[]
  children?: any
  className?: string
  footer?: JSX.Element
}

const Content = ({
  title,
  onBack,
  buttons,
  children,
  subTitle,
  endButtons,
  footer,
}: Props) => {
  let extraButtons: ContentButtonProps[] = []
  if (buttons) {
    extraButtons = [...extraButtons, ...buttons]
  }
  if (endButtons) {
    extraButtons = [...extraButtons, ...endButtons]
  }
  return (
    <>
      {title ? (
        <Head>
          <title>
            {title} - {SITE_NAME}
          </title>
        </Head>
      ) : null}
      {(title || onBack || subTitle || extraButtons.length > 0) && (
        <PageHeader
          onBack={onBack}
          ghost={false}
          title={title}
          subTitle={subTitle}
          extra={extraButtons.map((button, index) => {
            if (button.visible) {
              if (button.href)
                return (
                  <Link href={button.href} key={`end_button_${button.href}`}>
                    <Button
                      key={`content-button-${index}`}
                      type={button.type}
                      icon={button.icon}
                      className={button.className}
                    >
                      {button.text}
                    </Button>
                  </Link>
                )
              return (
                <Button
                  key={`content-button-${index}`}
                  type={button.type}
                  onClick={button.onClick}
                  icon={button.icon}
                  className={button.className}
                >
                  {button.text}
                </Button>
              )
            }
          })}
          footer={footer}
          className={'max-w-screen-2xl m-auto '}
        />
      )}
      <div className={'max-w-screen-2xl m-auto space-y-2'}>{children}</div>
    </>
  )
}

export default Content
