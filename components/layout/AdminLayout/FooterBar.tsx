import React, { FC } from 'react'
import { useRecoilValue } from 'recoil'
import { sidebarCollapsedState } from 'recoil/Atoms'
interface Props {
  left?: JSX.Element
  right?: JSX.Element
}

const FooterBar: FC<Props> = ({ left, right }) => {
  return (
    <div className={'ant-pro-footer-bar max-w-6xl m-auto right-auto -p-6'}>
      <div className={'ant-pro-footer-bar-left'}>{left}</div>
      <div className={'ant-pro-footer-bar-right'}>{right}</div>
    </div>
  )
}
export default FooterBar
