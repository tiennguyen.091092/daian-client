import {EnvironmentOutlined, HomeOutlined, UserOutlined,} from '@ant-design/icons'
import Sider from 'antd/lib/layout/Sider'
import Menu from 'antd/lib/menu'
import {useRouter} from 'next/router'
import React, {FC, useEffect, useState} from 'react'
import {useRecoilState, useRecoilValue} from 'recoil'
import {parseCookies} from 'nookies'
import Link from 'common/Link'
import {_findInTree, hasPermission} from 'utils'
import {authState, sidebarCollapsedState} from 'recoil/Atoms'
import { MdOutlineChecklist,MdLocalShipping,MdGroups } from 'react-icons/md'

export interface SidebarItem {
  title: string
  icon?: JSX.Element
  visible?: boolean
  path?: string
  key?: string
  children?: SidebarItem[]
  permissions?: string[]
}

interface ISideBar {
  drawerVisible?: boolean
}

const Sidebar: FC<ISideBar> = () => {
  const router = useRouter()
  const user = useRecoilValue(authState)
  const { isSidebarCollapsed } = parseCookies(null)
  const [openKeys, setOpenKeys] = useState<string[] | undefined>([])
  const [selectedKeys, setSelectedKeys] = useState<string[] | undefined>([])
  const [routes, setRoutes] = useState<SidebarItem[]>([])
  const [sidebarCollapsed, setSidebarCollapsed] = useRecoilState(
    sidebarCollapsedState
  )
  useEffect(() => {
    setRoutes([
      {
        title: 'Trang chủ',
        icon: <HomeOutlined />,
        visible: true,
        key: 'home',
        path: '/',
      },
      {
        title: 'Tài khoản',
        icon:<UserOutlined />,
        key: 'user',
        path: '/user',
        visible: hasPermission(user),
      },
      {
        title: 'Địa điểm',
        icon: <EnvironmentOutlined />,
        visible: hasPermission(user, [
          'location_createOneBase',
          'location_deleteOneBase',
          'location_getManyBase',
          'location_getOneBase',
          'location_createOneBase',
        ]),
        key: 'location',
        path: '/location',
      },
      {
        title: 'Văn phòng',
        icon: <HomeOutlined />,
        visible: hasPermission(user, [
          'branch-office_createOneBase',
          'branch-office_deleteOneBase',
          'branch-office_getManyBase',
          'branch-office_getOneBase',
          'branch-office_createOneBase',
        ]),
        key: 'home',
        path: '/branch-office',
      },
      {
        title: 'Khách hàng',
        icon:<MdGroups />,
        visible: hasPermission(user, [
          'customer_createOneBase',
          'customer_deleteOneBase',
          'customer_getManyBase',
          'customer_getOneBase',
          'customer_createOneBase',
        ]),
        key: 'customer',
        path: '/customer',
      },
      {
        title: 'Xe - Tài xế',
        icon: <MdLocalShipping />,
        visible: hasPermission(user, [
          'driver-car_createOneBase',
          'driver-car_deleteOneBase',
          'driver-car_getManyBase',
          'driver-car_getOneBase',
          'driver-car_createOneBase',
        ]),
        key: 'driver-car',
        path: '/driver-car',
      },
      {
        title: 'Đơn hàng',
        icon: <MdOutlineChecklist />,
        visible: true,
        key: 'order',
        path: '/order',
      },
      {
        title: 'Chuyến hàng',
        icon: <MdOutlineChecklist />,
        visible: true,
        key: 'on-order',
        path: '/on-order',
      },
    ])
  }, [user])

  useEffect(() => {
    routes.map((route) => {
      const tmpOpenKeys = _findInTree(route, router.asPath)
      if (tmpOpenKeys) {
        setOpenKeys(tmpOpenKeys)
        setSelectedKeys([router.asPath])
      }
      // else if(tmpOpenKeys===undefined){
      //   let parts = router.asPath.split("/");
      //   setSelectedKeys([`/${parts[1]}`])
      // }
    })
  }, [router.asPath])


  useEffect(() => {
    setSidebarCollapsed(
      isSidebarCollapsed === undefined ? false : isSidebarCollapsed !== '1'
    )
  }, [isSidebarCollapsed])
  const submenuChild = (obj: SidebarItem) => {
    obj?.permissions?.map((permission) => {
      if (user?.permissions?.includes(permission)) obj.visible = true
    })
    if (user?.roleId == 1) obj.visible = true
    if (obj.children && obj.children.length > 0) {
      const cHtml = obj.children.map((item) => {
        return submenuChild(item)
      })
      return (
        obj.visible && (
          <Menu.SubMenu
            key={obj.path || obj.key}
            title={obj.title}
            icon={obj.icon}
          >
            {cHtml}
          </Menu.SubMenu>
        )
      )
    } else {
      return (
        obj.visible && (
          <Menu.Item key={obj.path || obj.key} icon={obj.icon}>
            <Link href={obj?.path || '#'}>{obj.title}</Link>
          </Menu.Item>
        )
      )
    }
  }

  const menu = routes.map((obj) => {
    obj?.permissions?.map((permission) => {
      if (user?.permissions?.includes(permission)) obj.visible = true
    })
    if (user?.roleId == 1) obj.visible = true
    if (obj.children && obj.children.length > 0) {
      return submenuChild(obj)
    } else {
      return (
        obj.visible && (
          <Menu.Item key={obj.path || obj.key} icon={obj.icon}>
            <Link href={obj?.path || '#'}>{obj.title}</Link>
          </Menu.Item>
        )
      )
    }
  })

  const handleOpenChangeMenu = (openKeys: React.Key[]) => {
   setOpenKeys(openKeys as string[])
  }

  return (
    <>
      <Sider
        trigger={null}
        breakpoint="lg"
        collapsible
        collapsed={sidebarCollapsed}
        style={{
          overflow: 'auto',
          height: '100vh',
          position: 'fixed',
          left: 0,
        }}
      >
        <Link
          href={{
            pathname: '/',
          }}
        >
          <div className="slider-logo block m-3 text-center">
            <img src="../../images/logo-ngang.png" alt="Logo" width={160} className={'mx-auto'} />
          </div>
        </Link>
        <Menu
          theme="dark"
          mode="inline"
          selectedKeys={[router.asPath]}
          openKeys={openKeys}
          onOpenChange={handleOpenChangeMenu}
          className={"p-1"}
        >
          {menu}
        </Menu>
      </Sider>
    </>
  )
}
export default Sidebar
