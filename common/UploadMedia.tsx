import React, { FC, useEffect, useState } from 'react'
import { Modal, Upload } from 'antd'
import { UploadOutlined } from '@ant-design/icons'
import ReactPlayer from 'react-player'
import { UploadRequestOption as RcCustomRequestOptions } from 'rc-upload/lib/interface'
import { RcFile } from 'antd/lib/upload'
import { alertError, getCdnFile } from 'utils'
import { UploadFile } from 'antd/es/upload/interface'
import { useSetRecoilState } from 'recoil'
import { loadingState } from 'recoil/Atoms'
import mime from 'mime-types'
import { CdnService } from 'services'

interface IProps {
  files?: string[]
  onImageChange?: React.Dispatch<React.SetStateAction<string[]>>
  onUploaded?: (url: string) => void
  onRemoved?: () => void
  multiple?: boolean
}

const UploadMedia: FC<IProps> = (props) => {
  const { files, onImageChange, multiple = true, onUploaded, onRemoved } = props
  const setIsLoading = useSetRecoilState(loadingState)
  const [fileList, setFileList] = useState<UploadFile[]>([])
  const [previewVisible, setPreviewVisible] = useState<boolean>(false)
  const [previewFile, setPreviewFile] = useState<UploadFile>()

  useEffect(() => {
    if (files?.length) {
      setFileList(() => {
        return files.map((file) => {
          const fileParts = file.split('/')
          const fileName = fileParts[fileParts.length - 1]
          return {
            uid: file,
            size: 0,
            name: file,
            url: getCdnFile(file),
            status: 'done',
            originFileObj: {} as RcFile,
            type: mime.lookup(fileName.split('?')[0]) as string,
          }
        })
      })
    } else {
      setFileList([])
    }
  }, [files])

  const handlePreview = (file: UploadFile) => {
    setPreviewVisible(!previewVisible)
    setPreviewFile(file)
  }

  const handleMediaUpload = (file) => {
    if (
      file.type !== 'image/png' &&
      file.type !== 'image/jpg' &&
      file.type !== 'image/jpeg' &&
      file.type !== 'video/mp4'
    ) {
      alertError(`Chỉ được upload file ảnh(.PNG, .JPG, .PNG) hoặc video(.MP4)`)
    }
    return file.type === 'image/png' ||
      file.type === 'image/jpg' ||
      file.type === 'image/jpeg' ||
      file.type === 'video/mp4'
      ? true
      : Upload.LIST_IGNORE
  }
  const uploadImage = (options: RcCustomRequestOptions) => {
    setIsLoading(true)
    const { onSuccess, onError, file } = options

    CdnService.cdnControllerUploadFile({ files: file })
      .then((response) => {
        onSuccess('Ok')
        if (!multiple && onUploaded) {
          onUploaded(response.files[0])
        } else {
          onImageChange((prevState) => {
            if (prevState) return [...prevState, response.files[0]]
            else return [response.files[0]]
          })
        }
        setIsLoading(false)
      })
      .catch((error) => {
        alertError(error)
        onError(error)
        setIsLoading(false)
      })
  }

  return (
    <>
      <Upload
        customRequest={uploadImage}
        listType="picture-card"
        fileList={fileList}
        maxCount={1}
        onPreview={handlePreview}
        multiple={multiple}
        beforeUpload={handleMediaUpload}
        onRemove={(file) => {
          if (!onUploaded) {
            onImageChange((prevState) => {
              const tmpFiles = [...prevState]
              const existIndex = tmpFiles.findIndex(
                (tmpFile) => tmpFile === file.name
              )
              if (existIndex > -1) tmpFiles.splice(existIndex, 1)
              if (tmpFiles.length === 0) setFileList(tmpFiles)
              return tmpFiles
            })
          } else {
            onUploaded('')
          }
          return true
        }}
      >
        {(multiple || (!multiple && fileList?.length === 0)) && (
          <div>
            <UploadOutlined />
            <div style={{ marginTop: 8 }}>Tải lên</div>
          </div>
        )}
      </Upload>
      <Modal
        visible={previewVisible}
        title={previewFile?.name}
        footer={null}
        onCancel={() => {
          setPreviewVisible(!previewVisible)
        }}
      >
        {previewFile?.type !== 'image/png' &&
        previewFile?.type !== 'image/jpg' &&
        previewFile?.type !== 'image/jpeg' ? (
          <ReactPlayer
            width={'100%'}
            src={getCdnFile(previewFile?.url)}
            playing={true}
          />
        ) : (
          <img
            alt={'Ảnh'}
            style={{ width: '100%' }}
            src={getCdnFile(previewFile?.url)}
          />
        )}
      </Modal>
    </>
  )
}
export default UploadMedia
