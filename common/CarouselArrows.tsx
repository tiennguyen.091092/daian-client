import { ChevronIcon } from 'constants/icons'
import React from 'react'

export const NextArrow = (props) => {
  const { className, style, onClick } = props
  return (
    <div className={className} style={{ ...style }} onClick={onClick}>
      <ChevronIcon fill={'#FFFFFF'} width={10} height={18} />
    </div>
  )
}

export const PrevArrow = (props) => {
  const { className, style, onClick } = props
  return (
    <div
      className={className}
      style={{ ...style, transform: 'rotate(180deg)' }}
      onClick={onClick}
    >
      <ChevronIcon fill={'#FFFFFF'} width={10} height={18} />
    </div>
  )
}
