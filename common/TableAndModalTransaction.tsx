import React, { FC, useEffect, useState } from 'react'
import { Modal } from 'antd'
import {
  EnumUserTransactionEntityTransactionType,
  UserTransactionEntity,
  UserTransactionService,
} from 'services'
import { alertError } from 'utils'
import { ExclamationCircleOutlined } from '@ant-design/icons'
import DataTable from 'components/common/DataTable'
import { ColumnsType } from 'antd/es/table'

interface IProps {
  roleUser?: string
  transactionType?: string
  redirectUrl?: string
  tableServiceParams?: any
  columns: any[]
  cardTitle?: string
  isDownloadable?: boolean
  isBorderTable?: boolean
  columnTransactionDetail?: ColumnsType<UserTransactionEntity>
  tableScroll?: number
}

const TableAndModalTransaction: FC<IProps> = (props) => {
  const [transactionId, setTransactionId] = useState<number>(0)
  const [title, setTitle] = useState<string>('Nạp tiền')

  const [transactionDetail, setTransactionDetail] =
    useState<UserTransactionEntity>()
  useEffect(() => {
    if (
      props.transactionType ===
      EnumUserTransactionEntityTransactionType.withdrawal
    ) {
      setTitle('Rút tiền')
    }
  }, [])
  useEffect(() => {
    if (transactionId && transactionId > 0) {
      UserTransactionService.getOneBase({
        id: Number(transactionId),
        join: ['user', 'modifiedUser'],
      })
        .then((response) => {
          setTransactionDetail(response)
        })
        .catch((e) => {
          alertError(e)
        })
    }
  }, [transactionId])
  const { confirm } = Modal
  const showConfirm = () => {
    confirm({
      title: `Bạn có chắc chắn muốn xác nhận ${title}?`,
      icon: <ExclamationCircleOutlined />,
      content: '',
      onOk() {
        // UserTransactionService.transactionControllerVerify({
        //   id: transactionId,
        // })
        //   .then(() => {
        //     alertSuccess(`Xác nhận ${title} thành công`)
        //     setTimeout(() => {
        //       router.reload()
        //     }, 1000)
        //   })
        //   .catch((error) => alertError(error))
      },
    })
  }
  const showConfirmCancel = () => {
    confirm({
      title: `Bạn có chắc chắn muốn hủy ${title}?`,
      icon: <ExclamationCircleOutlined />,
      content: '',
      // onOk() {
      //   TransactionService.transactionControllerCancel({
      //     id: transactionId,
      //   })
      //     .then(() => {
      //       alertSuccess(`Hủy ${title} thành công`)
      //       router.reload()
      //     })
      //     .catch((error) => alertError(error))
      // },
    })
  }
  // const tabTransactionChange = (key) => {
  //   if (key === 'tab-transaction-list-detail') {
  //     UserTransactionService.getManyBase({
  //       filter: [
  //         `type||eq||${props.transactionType}`,
  //         `userId||eq||${transactionDetail.userId}`,
  //       ],
  //       sort: ['createdAt,DESC'],
  //       join: ['user', 'modifiedUser'],
  //     })
  //       .then((timeResponse) => {
  //         setTransactionDetailListData(timeResponse.data)
  //       })
  //       .catch((error: any) => {
  //         alertError(error)
  //       })
  //   }
  // }
  return (
    <>
      <DataTable
        scroll={{
          x: props.tableScroll,
        }}
        bordered={props.isBorderTable || false}
        service={UserTransactionService.getManyBase}
        serviceParams={props.tableServiceParams}
        columns={props.columns}
        // action={{
        //   buttons: [
        //     hasPermission(user, [
        //       'transaction_createOneBase',
        //       'transaction_updateOneBase',
        //     ]) && {
        //       type: 'view',
        //       onClick: (id) => {
        //         setTransactionId(id)
        //         return setIsHistoryDetailVisible(true)
        //       },
        //     },
        //   ],
        // }}
        cardTitle={props.cardTitle}
        downloadable={props.isDownloadable}
      />
      {/*<Modal*/}
      {/*  title="Chi tiết giao dịch"*/}
      {/*  visible={isHistoryDetailVisible}*/}
      {/*  onCancel={() => {*/}
      {/*    setIsHistoryDetailVisible(false)*/}
      {/*    setTotalAmount(0)*/}
      {/*  }}*/}
      {/*  destroyOnClose={true}*/}
      {/*  width={1000}*/}
      {/*  footer={[*/}
      {/*    <>*/}
      {/*      {transactionDetail?.status ===*/}
      {/*      EnumTransactionEntityStatus.processing ? (*/}
      {/*        <>*/}
      {/*          {props.roleUser === 'admin' && (*/}
      {/*            <Button key="submit" type="primary" onClick={showConfirm}>*/}
      {/*              Xác nhận {title}*/}
      {/*            </Button>*/}
      {/*          )}*/}

      {/*          <Button*/}
      {/*            key="rollback"*/}
      {/*            type="primary"*/}
      {/*            danger*/}
      {/*            onClick={showConfirmCancel}*/}
      {/*          >*/}
      {/*            Hủy*/}
      {/*          </Button>*/}
      {/*        </>*/}
      {/*      ) : (*/}
      {/*        <></>*/}
      {/*      )}*/}
      {/*    </>,*/}
      {/*  ]}*/}
      {/*>*/}
      {/*  <Tabs defaultActiveKey="1" onChange={tabTransactionChange}>*/}
      {/*    <TabPane tab={`Chi tiết ${title}`} key="tab-transaction-detail">*/}
      {/*      <Form labelCol={{ span: 8 }} wrapperCol={{ span: 16 }}>*/}
      {/*        <Form.Item*/}
      {/*          label={'Khách hàng'}*/}
      {/*          style={{*/}
      {/*            marginBottom: '15px',*/}
      {/*            borderBottom: '1px solid #f0f0f0',*/}
      {/*          }}*/}
      {/*        >*/}
      {/*          <b>{transactionDetail?.user?.fullName}</b>*/}
      {/*        </Form.Item>*/}
      {/*        {props.transactionType === EnumTransactionEntityType.withdraw && (*/}
      {/*          <>*/}
      {/*            <Form.Item*/}
      {/*              label={'Tên ngân hàng'}*/}
      {/*              style={{*/}
      {/*                marginBottom: '15px',*/}
      {/*                borderBottom: '1px solid #f0f0f0',*/}
      {/*              }}*/}
      {/*            >*/}
      {/*              <b>{transactionDetail?.bankBranchName}</b>*/}
      {/*            </Form.Item>*/}
      {/*            <Form.Item*/}
      {/*              label={'Số tài khoản'}*/}
      {/*              style={{*/}
      {/*                marginBottom: '15px',*/}
      {/*                borderBottom: '1px solid #f0f0f0',*/}
      {/*              }}*/}
      {/*            >*/}
      {/*              <b>{transactionDetail?.bankNumber}</b>*/}
      {/*            </Form.Item>*/}
      {/*            <Form.Item*/}
      {/*              label={'Chủ tài khoản'}*/}
      {/*              style={{*/}
      {/*                marginBottom: '15px',*/}
      {/*                borderBottom: '1px solid #f0f0f0',*/}
      {/*              }}*/}
      {/*            >*/}
      {/*              <b>{transactionDetail?.bankAccountName}</b>*/}
      {/*            </Form.Item>*/}
      {/*          </>*/}
      {/*        )}*/}
      {/*        <Form.Item*/}
      {/*          label={'Số tiền'}*/}
      {/*          style={{*/}
      {/*            marginBottom: '15px',*/}
      {/*            borderBottom: '1px solid #f0f0f0',*/}
      {/*          }}*/}
      {/*        >*/}
      {/*          <b>{formatCurrency(transactionDetail?.amount)}</b>*/}
      {/*        </Form.Item>*/}
      {/*        <Form.Item*/}
      {/*          label={'Mô tả'}*/}
      {/*          style={{*/}
      {/*            marginBottom: '15px',*/}
      {/*            borderBottom: '1px solid #f0f0f0',*/}
      {/*          }}*/}
      {/*        >*/}
      {/*          {transactionDetail?.note}*/}
      {/*        </Form.Item>*/}
      {/*        <Form.Item*/}
      {/*          label={'Ngày tạo'}*/}
      {/*          style={{*/}
      {/*            marginBottom: '15px',*/}
      {/*            borderBottom: '1px solid #f0f0f0',*/}
      {/*          }}*/}
      {/*        >*/}
      {/*          {formatDate(transactionDetail?.createdAt)}*/}
      {/*        </Form.Item>*/}
      {/*        <Form.Item*/}
      {/*          label={'Trạng thái'}*/}
      {/*          style={{*/}
      {/*            marginBottom: '15px',*/}
      {/*            borderBottom: '1px solid #f0f0f0',*/}
      {/*          }}*/}
      {/*        >*/}
      {/*          {getStatusTransaction(transactionDetail?.status)}*/}
      {/*        </Form.Item>*/}
      {/*      </Form>*/}
      {/*    </TabPane>*/}
      {/*    {props.columnTransactionDetail && (*/}
      {/*      <TabPane*/}
      {/*        tab={*/}
      {/*          <span>*/}
      {/*            Tổng*/}
      {/*            {props.transactionType === EnumTransactionEntityType.topup*/}
      {/*              ? ' nạp'*/}
      {/*              : ' rút'}*/}
      {/*            {totalAmount > 0 ? (*/}
      {/*              <b>: {formatCurrency(totalAmount)}</b>*/}
      {/*            ) : (*/}
      {/*              ''*/}
      {/*            )}*/}
      {/*          </span>*/}
      {/*        }*/}
      {/*        key="tab-transaction-list-detail"*/}
      {/*      >*/}
      {/*        <div className={'flex'}>*/}
      {/*          <Table*/}
      {/*            dataSource={transactionDetailListData}*/}
      {/*            columns={props.columnTransactionDetail}*/}
      {/*            scroll={{ x: 500 }}*/}
      {/*            sticky*/}
      {/*            size={'small'}*/}
      {/*            pagination={false}*/}
      {/*            summary={(pageData) => {*/}
      {/*              let sumTotalAmount = 0*/}
      {/*              pageData.forEach((transaction) => {*/}
      {/*                sumTotalAmount += transaction?.amount || 0*/}
      {/*              })*/}
      {/*              setTotalAmount(sumTotalAmount)*/}
      {/*              return (*/}
      {/*                <>*/}
      {/*                  <Table.Summary.Row>*/}
      {/*                    <Table.Summary.Cell index={0}>*/}
      {/*                      <b>Tổng cộng</b>*/}
      {/*                    </Table.Summary.Cell>*/}
      {/*                    <Table.Summary.Cell index={1} className={'font-bold'}>*/}
      {/*                      {formatCurrency(sumTotalAmount)}*/}
      {/*                    </Table.Summary.Cell>*/}
      {/*                    <Table.Summary.Cell index={2} />*/}
      {/*                    <Table.Summary.Cell index={3} />*/}
      {/*                  </Table.Summary.Row>*/}
      {/*                </>*/}
      {/*              )*/}
      {/*            }}*/}
      {/*          />*/}
      {/*        </div>*/}
      {/*      </TabPane>*/}
      {/*    )}*/}
      {/*  </Tabs>*/}
      {/*</Modal>*/}
    </>
  )
}
export default TableAndModalTransaction
