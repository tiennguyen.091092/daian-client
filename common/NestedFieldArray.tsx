import React, { FC, useEffect } from 'react'
import { Controller, useFieldArray, useWatch } from 'react-hook-form'
import { Button, Col, Input, Row } from 'antd'
import { DeleteOutlined, PlusOutlined } from '@ant-design/icons'

const NestedFieldArray: FC<any> = ({ nestIndex, control, register }) => {
  useEffect(() => {}, [watchGroupTypeName])

  const watchGroupTypeName = useWatch({
    control,
    name: `product.groupType.${nestIndex}.name`,
  })
  const { fields, remove, append } = useFieldArray({
    control,
    name: `product.groupType[${nestIndex}].value`,
  })
  useEffect(() => {
    // Update the document title using the browser API
    append({
      field: '',
    })
  }, [])

  return (
    <>
      {fields.map((item, k) => {
        return (
          <Row
            key={item.id}
            gutter={[16, 16]}
            className={'mb-2'}
            style={{
              width: '100%',
            }}
          >
            <Col xs={24} md={22}>
              <Controller
                control={control}
                name={`variant[${nestIndex}].options[${k}].field`}
                render={({ field }) => (
                  <Input {...field} placeholder={`Nhập phân loại hàng`} />
                )}
              />
            </Col>
            <Col xs={24} md={2}>
              {fields.length > 1 ? (
                <Button
                  shape={'circle'}
                  onClick={() => remove(k)}
                  icon={<DeleteOutlined />}
                />
              ) : null}
            </Col>
          </Row>
        )
      })}
      <Row gutter={[16, 16]}>
        <Col xs={24} md={24}>
          <Button
            type="dashed"
            onClick={() => append({})}
            style={{
              width: '90%',
              color: '#5863dc',
              borderColor: '#5863dc',
            }}
            icon={<PlusOutlined />}
          >
            Thêm phân loại hàng
          </Button>
        </Col>
        <Col xs={24} md={2}></Col>
      </Row>
    </>
  )
}
export default NestedFieldArray
