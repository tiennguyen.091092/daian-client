import * as React from 'react'
import { FC, useEffect, useRef, useState } from 'react'
import 'react-quill/dist/quill.snow.css'
import dynamic from 'next/dynamic'
import { axios } from 'services/serviceOptions'
import { alertError, getCdnFile } from 'utils'
import { ReactQuillProps } from 'react-quill/lib'

interface Props {
  name?: string
  value?: string
  defaultValue?: string
  onChange?: (value) => void
}

const ReactQuill: React.ComponentType<any> = dynamic(
  async () => {
    const { default: RQ } = await import('react-quill')

    return ({ forwardedRef, ...props }) => <RQ ref={forwardedRef} {...props} />
  },
  { ssr: false }
)

const RichTextEditor: FC<Props> = (props) => {
  const [editorHtml, setEditorHtml] = useState('')
  const editorRef = useRef<ReactQuillProps>()
  const imageHandler = () => {
    const input = document.createElement('input')

    input.setAttribute('type', 'file')
    input.setAttribute('accept', 'image/*')
    input.click()

    input.onchange = () => {
      const file: any = input.files[0]
      const fmData = new FormData()
      fmData.append('file', file)
      axios(
        {
          data: fmData,
          method: 'POST',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          url: '/cdn/upload',
        },
        (response) => {
          const imageUrl = `${getCdnFile(response.files[0])}`
          // @ts-ignore
          const range = editorRef.current.getEditorSelection()
          editorRef.current
            // @ts-ignore
            .getEditor()
            .insertEmbed(range.index, 'image', imageUrl)
        },
        (error) => {
          alertError(error)
        }
      ).then()
    }
  }
  const modules = React.useMemo(
    () => ({
      toolbar: {
        container: [
          [{ font: [] }, { size: [] }],
          [{ align: [] }, 'direction'],
          ['bold', 'italic', 'underline', 'strike'],
          [{ color: [] }, { background: [] }],
          [{ script: 'super' }, { script: 'sub' }],
          ['blockquote', 'code-block'],
          [
            { list: 'ordered' },
            { list: 'bullet' },
            { indent: '-1' },
            { indent: '+1' },
          ],
          ['link', 'image', 'video'],
          ['clean'],
        ],
        handlers: {
          image: imageHandler,
        },
      },
    }),
    []
  )

  useEffect(() => {
    setEditorHtml(props.value)
  }, [props.value])

  const handleChange = (html) => {
    setEditorHtml(html)
    props.onChange(html)
  }

  return (
    <ReactQuill
      name={props.name}
      onChange={handleChange}
      forwardedRef={editorRef}
      modules={modules}
      value={editorHtml || ''}
    />
  )
}

export default RichTextEditor
