import { FC } from 'react'
import {
  SortableContainer,
  SortableElement,
  SortableHandle,
} from 'react-sortable-hoc'

const DragHandle = SortableHandle(() => <span>::</span>)

const SortableItem = SortableElement(({ value }) => (
  <li>
    <DragHandle />
    {value}
  </li>
))

const Sortable = SortableContainer(({ children }) => {
  return <ul>{children}</ul>
})

export interface Item {
  label: string
  value: string
}

interface Props {
  items: Item[]
  onSortEnd?: any
}

const SortableList: FC<Props> = ({ items, onSortEnd }) => {
  const _onSortEnd = ({ oldIndex, newIndex }) => {
    // this.setState(({items}) => ({
    //   items: arrayMove(items, oldIndex, newIndex),
    // }));
  }
  return (
    <Sortable onSortEnd={_onSortEnd} useDragHandle>
      {items &&
        items.map((item, index) => (
          <SortableItem
            key={`item-${item.value}`}
            index={index}
            value={item.label}
          />
        ))}
    </Sortable>
  )
}
export default SortableList
