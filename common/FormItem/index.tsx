import { Form, FormItemProps } from 'antd'
import React from 'react'

const FormItem: React.FC<FormItemProps> = (props) => {
  return (
    <Form.Item
      labelCol={{
        md: { span: 8 },
        sm: {
          span: 12,
        },
      }}
      {...props}
    >
      {props.children}
    </Form.Item>
  )
}

export default FormItem
