import React, { FC, useEffect, useState } from 'react'
import { Controller, useFormContext } from 'react-hook-form'
import { Col, Input, Row } from 'antd'
import InputNumber from 'components/common/InputNumber'
import { MerchantAddressEntity } from 'services'
import MerchantAddress from 'components/common/Variant/Address'

interface IVariantTableBody {
  productGroupType: any
  merchantAddress: MerchantAddressEntity[]
}

const VariantTableBody: FC<IVariantTableBody> = (props) => {
  const { control, setValue } = useFormContext()
  const { productGroupType, merchantAddress } = props

  return (
    <>
      {productGroupType?.[0]?.value?.map(
        (productGroupTypeItem, productGroupTypeIndex) => {
          setValue(
            `variants.${productGroupTypeIndex}.name`,
            productGroupTypeItem?.field
          )
          setValue(`variants.${productGroupTypeIndex}.groupTypeIndex`, 0)
          setValue(
            `variants.${productGroupTypeIndex}.valueTypeIndex`,
            productGroupTypeIndex
          )
          if (productGroupTypeItem?.field)
            return (
              <Row
                className={'text-center'}
                key={`productGroupTypeItem-${productGroupTypeIndex}`}
              >
                {/*Phân loại 1*/}
                <Col
                  span={6}
                  className={
                    'border-l border-b border-r py-2 flex items-center text-center justify-center '
                  }
                  key={`productGroupTypeItemValue-1-${productGroupTypeIndex}`}
                >
                  {productGroupTypeItem?.field}
                </Col>
                {/*Nếu có Phân loại 2*/}
                {productGroupType?.[1]?.value?.length > 0 ? (
                  <>
                    <Col span={6} className={'flex m-h-full flex-col'}>
                      {productGroupType?.[1]?.value.map(
                        (
                          productSecondGroupTypeItem,
                          productSecondGroupTypeIndex
                        ) => {
                          if (productSecondGroupTypeItem?.field) {
                            setValue(
                              `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.name`,
                              `${productGroupTypeItem?.field}, ${productSecondGroupTypeItem?.field}`
                            )
                            setValue(
                              `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.groupTypeIndex`,
                              productGroupTypeIndex
                            )
                            setValue(
                              `variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.valueTypeIndex`,
                              productSecondGroupTypeIndex
                            )
                            return (
                              <div
                                className={
                                  'border-b border-r py-2 flex items-center justify-center flex-1'
                                }
                                key={`productGroupTypeItemValue-2-${productSecondGroupTypeIndex}`}
                              >
                                <Controller
                                  control={control}
                                  name={`variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.name`}
                                  render={({ field }) => (
                                    <Input {...field} type={'hidden'} />
                                  )}
                                />
                                {productSecondGroupTypeItem?.field}
                              </div>
                            )
                          }
                        }
                      )}
                    </Col>
                    <Col span={4} className={'flex m-h-full flex-col'}>
                      {productGroupType?.[1]?.value?.map(
                        (
                          productSecondGroupTypeItem,
                          productSecondGroupTypeIndex
                        ) => {
                          if (productSecondGroupTypeItem?.field)
                            return (
                              <div
                                className={
                                  'border-b border-r py-2 flex items-center justify-center flex-1'
                                }
                                key={`productGroupTypeItemValue-price-${productSecondGroupTypeIndex}`}
                              >
                                <div className={'product-price'}>
                                  <Controller
                                    control={control}
                                    name={`variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.price`}
                                    render={({ field }) => (
                                      <InputNumber
                                        {...field}
                                        placeholder="Nhập vào"
                                        addonBefore={'đ'}
                                        className={'border-none w-full'}
                                      />
                                    )}
                                  />
                                </div>
                                <Controller
                                  control={control}
                                  name={`variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.groupTypeIndex`}
                                  render={({ field }) => (
                                    <Input
                                      {...field}
                                      value={productGroupTypeIndex}
                                      type={'hidden'}
                                    />
                                  )}
                                />
                                <Controller
                                  control={control}
                                  name={`variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.valueTypeIndex`}
                                  render={({ field }) => (
                                    <Input
                                      {...field}
                                      value={productSecondGroupTypeIndex}
                                      type={'hidden'}
                                    />
                                  )}
                                />
                              </div>
                            )
                        }
                      )}
                    </Col>
                    <Col span={8} className={'flex m-h-full flex-col'}>
                      {productGroupType?.[1]?.value?.map(
                        (
                          productSecondGroupTypeItem,
                          productSecondGroupTypeIndex
                        ) => {
                          if (productSecondGroupTypeItem?.field)
                            return (
                              <MerchantAddress
                                inputPrefixName={`variants.${productGroupTypeIndex}.${productSecondGroupTypeIndex}.stock`}
                                merchantAddresses={merchantAddress}
                              />
                            )
                        }
                      )}
                    </Col>
                  </>
                ) : (
                  <>
                    <Col span={6} className={'flex m-h-full flex-col'}>
                      <div
                        className={
                          'border-b border-r py-2 flex items-center justify-center flex-1'
                        }
                      >
                        <Controller
                          control={control}
                          name={`variants.${productGroupTypeIndex}.name`}
                          render={({ field }) => (
                            <Input {...field} type={'hidden'} />
                          )}
                        />
                        <div className={'product-price'}>
                          <Controller
                            control={control}
                            name={`variants.${productGroupTypeIndex}.price`}
                            render={({ field }) => (
                              <InputNumber
                                {...field}
                                placeholder="Nhập vào"
                                addonBefore={'đ'}
                                className={'border-none w-full'}
                              />
                            )}
                          />
                        </div>
                        <Controller
                          control={control}
                          name={`variants.${productGroupTypeIndex}.groupTypeIndex`}
                          render={({ field }) => (
                            <Input {...field} type={'hidden'} value={0} />
                          )}
                        />
                        <Controller
                          control={control}
                          name={`variants.${productGroupTypeIndex}.valueTypeIndex`}
                          render={({ field }) => (
                            <Input
                              {...field}
                              type={'hidden'}
                              value={productGroupTypeIndex}
                            />
                          )}
                        />
                      </div>
                    </Col>
                    <Col span={12} className={'flex m-h-full flex-col'}>
                      <MerchantAddress
                        inputPrefixName={`variants.${productGroupTypeIndex}.stock`}
                        merchantAddresses={merchantAddress}
                      />
                    </Col>
                  </>
                )}
              </Row>
            )
        }
      )}
    </>
  )
}
export default VariantTableBody
