import React from 'react'
import { Slider } from 'antd'

// just some cosmetics
const prettyInt = (x) =>
  Math.round(x)
    .toString()
    .replace(/\B(?=(\d{3})+(?!\d))/g, ',')

// change these to whatever curve function you need!
export const sliderCurve = Math.exp
export const inverseCurve = Math.log
const makeMarks = (a) =>
  a.reduce((acc, cur) => {
    acc[inverseCurve(cur)] = prettyInt(cur)
    return acc
  }, {})

const LogSlider = ({ min, max, marks, stepsInRange }) => {
  console.log(makeMarks(marks));
  return <Slider
    min={inverseCurve(min)}
    max={inverseCurve(max)}
    marks={makeMarks(marks)}
    step={(inverseCurve(max) - inverseCurve(min)) / stepsInRange}
    defaultValue={27000000}
    tipFormatter={(value) => prettyInt(sliderCurve(value))}
  />
}

export default LogSlider
