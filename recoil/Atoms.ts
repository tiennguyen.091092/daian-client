import { atom } from 'recoil'
import { JwtPayload } from 'jwt-decode'
// import { ProductCategoryEntity, ProductEntity } from 'services'
//
// export interface CartProduct extends ProductEntity {
//   id?: number
//   quantity?: number
//   checked?: boolean
//   walletPackageId?: number
// }

// export interface CartState {
//   [id: number]: CartProduct
// }

export interface Auth extends JwtPayload {
  tel: string
  fullName?: string
  referralCode?: string
  permissions: string[]
  roleId: number
  id:number
}

export const authState = atom<Auth>({
  key: 'AuthState',
  default: null,
})
export const drawerVisibleState = atom<boolean>({
  key: 'DrawerVisible',
  default: false,
})

export const loadingState = atom<boolean>({
  key: 'LoadingState',
  default: false,
})

export const sidebarCollapsedState = atom<boolean>({
  key: 'SidebarCollapsedState',
  default: false,
})
export const leftDrawerState = atom<boolean>({
  key: 'LeftDrawerState',
  default: false,
})
export const countDataTable = atom<number>({
  key: 'CountDataTable',
  default: 0,
})
// export const productCategoryState = atom<ProductCategoryEntity[]>({
//   key: 'ProductCategory',
//   default: [],
// })
// export const cartState = atom<CartState>({
//   key: 'CartState',
//   default: {},
// })
// export const showAddToCartSuccessState = atom<boolean>({
//   key: 'ShowAddToCartSuccessState',
//   default: false,
// })
