import { useRouter } from 'next/router'
import { parseCookies } from 'nookies'
import React, { useEffect, useState } from 'react'
import { useRecoilState } from 'recoil'
import axios, { AxiosRequestConfig } from 'axios'
import { serviceOptions } from 'services/serviceOptions'
import { Auth, authState } from 'recoil/Atoms'
import jwt_decode from 'jwt-decode'
import { stringify } from 'qs'
import Loading from 'common/Loading'

const withAuth = (WrappedComponent: any) => {
  return (props: any) => {
    const router = useRouter()
    const { token } = parseCookies(null)
    const publicLayout = [''].includes(router.pathname)

    const [axiosConfig, setAxiosConfig] = useState<AxiosRequestConfig>({
      baseURL: process.env.NEXT_PUBLIC_API_URL || 'http://localhost:4000/',
      timeout: 60000, // 1 phút
      paramsSerializer: (params) =>
        stringify(params, { arrayFormat: 'repeat' }),
    })

    const [user, setUser] = useRecoilState(authState)

    serviceOptions.axios = axios.create(axiosConfig)
    useEffect(() => {
      serviceOptions.axios = axios.create(axiosConfig)
    }, [axiosConfig])

    useEffect(() => {
      if (!token && !publicLayout) window.location.href = '/auth/login'
      else if (token) {
        const userTokenData = jwt_decode<Auth>(token)
        if (userTokenData) {
          const currentTime = new Date().getTime() / 1000
          if (currentTime > userTokenData?.exp) window.location.href = '/auth/login'

          setUser(userTokenData)

          if (token) {
            setAxiosConfig((prevConfig) => ({
              ...prevConfig,
              headers: {
                Authorization: `Bearer ${token}`,
                'Access-Control-Allow-Origin': '*',
              },
            }))
          }
        }
      } else {
        serviceOptions.axios = axios.create(axiosConfig)
      }
    }, [router.pathname])

    if (!user?.sub && !publicLayout) return <Loading />

    return <WrappedComponent {...props} />
  }
}
export default withAuth
