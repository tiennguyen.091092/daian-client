import {NextPage} from 'next'
import {AppProps} from 'next/app'
import Head from 'next/head'
import {ConfigProvider} from 'antd'
import viVN from 'antd/lib/locale/vi_VN'
import {useRouter} from 'next/router'
import React, {useEffect} from 'react'
import {RecoilRoot} from 'recoil'
import {SITE_NAME, SITE_SLOGAN} from '@/constants'
import AdminLayout from 'components/layout/AdminLayout'
// import '../styles/customize-taildwincss.css'
import '../styles/admin.styles.css'
import {GoogleReCaptchaProvider} from 'react-google-recaptcha-v3'

const MyApp: NextPage<AppProps> = ({ Component, pageProps }) => {
  const router = useRouter()
  const authLayout = ['/auth/login', '/auth/register'].includes(router.pathname)

  useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side')
    if (jssStyles) {
      jssStyles.parentElement?.removeChild(jssStyles)
    }
    // initFacebookSdk().then()
  }, [])

  return (
      <>
        <Head>
          <title>
            {SITE_NAME} - {SITE_SLOGAN}
          </title>
          <meta
              name="viewport"
              content="minimum-scale=1, initial-scale=1, width=device-width"
          />
          <link
              rel="preconnect"
              href="https://fonts.gstatic.com"
              crossOrigin={'anonymous'}
          />
          <link
              href="https://fonts.googleapis.com/css2?family=Asap:wght@400;500;600;700&display=swap"
              rel="stylesheet"
          />
        </Head>
        <GoogleReCaptchaProvider
            reCaptchaKey={'AIzaSyBZUyfNngK4CsLJfaJfDNjdbBe8A4qBN6E'}
            scriptProps={{ async: true }}
            language={'vi'}
            useRecaptchaNet={true}
        >
          <RecoilRoot>
            <ConfigProvider locale={viVN}>
              {authLayout ? (
                  // @ts-ignore
                  <Component {...pageProps} />
              )  : (
                  // @ts-ignore
                  <AdminLayout> <Component {...pageProps} /></AdminLayout>
              )}
            </ConfigProvider>
          </RecoilRoot>
        </GoogleReCaptchaProvider>
      </>
  )
}

export default MyApp
