import {
    BranchOfficeEntity,
    BranchOfficeService,
    CreateOrderDto,
    CustomerEntity,
    CustomerService,
    EnumCreateOrderDtoPaymentType,
    EnumOrderEntityPaymentType,
    LocationEntity,
    LocationService,
    OrderService
} from 'services'
import React, {FC, useEffect, useMemo, useState} from 'react'
import {useRecoilValue, useSetRecoilState} from 'recoil'
import {authState, loadingState} from 'recoil/Atoms'
import {useRouter} from 'next/router'
import {Controller, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {yupResolver} from '@hookform/resolvers/yup'
import * as yup from 'yup'
import {alertError, alertSuccess, filterOption, formatCurrency, modifyEntity} from 'utils'
import Content from 'components/layout/AdminLayout/Content'
import {AutoComplete, Button, Card, Col, Form, Input, Row, Select, Tooltip, Typography} from 'antd'
import InputNumber from 'common/InputNumber'
import {PrinterOutlined} from "@ant-design/icons";


interface Inputs {
    order: CreateOrderDto
    customerSendName?: string
    customerSendPhone?: {
        value: string
    }
    customerReceivePhone?: {
        value: string
    }
    customerReceiveName?: string
    // customerReceivePhone?: string
    customerReceiveAddress?: string
}

// const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema = yup.object().shape({
    customerSendName: yup.string().required('Chưa nhập tên người gửi'),
    customerReceiveName: yup.string().required('Chưa nhập tên người gửi'),
    customerSendPhone: yup.object().shape({
        value: yup.string()
            .required('Chưa nhập số điện thoại người nhận')
    }),
    customerReceivePhone: yup.object().shape({
        value: yup.string()
            .required('Chưa nhập số điện thoại người gửi')
    }),
    order: yup.object().shape({
        locationId: yup.string().required('Chưa chọn điểm giao'),
        branchOfficeSendId: yup.string().required('Chưa chọn VP gửi'),
        branchOfficeReceiveId: yup.string().required('Chưa chọn VP nhận'),
        itemName: yup.string().required('Chưa nhập Tên hàng hóa'),
        quantity: yup
            .number()
            .min(0, 'Vui lòng nhập vào giá trị giữa 0 và 1000 .')
            .max(1000, 'Vui lòng nhập vào giá trị giữa 1 và 1000 .'),
        weight: yup
            .number()
            .min(0, 'Vui lòng nhập vào giá trị giữa 0 và 1000 .')
            .max(1000, 'Vui lòng nhập vào giá trị giữa 1 và 1000 .'),
        orderMoney: yup
            .number()
            .min(1000, 'Vui lòng nhập tối thiểu 1000đ.')
            .required('Không được để trống ô'),
        // address: yup.string().required('Chưa nhập địa chỉ giao hàng'),
    }),
})
const EditOrder: FC = () => {
    const [title, setTitle] = useState<string>('Cập nhật đơn hàng')
    const user = useRecoilValue(authState)
    const [locations, setLocations] = useState<LocationEntity[]>()
    const [customers, setCustomers] = useState<CustomerEntity[]>()
    const [branchOffices, setBranchOffices] = useState<BranchOfficeEntity[]>()
    const [optionCustomers, setOptionCustomers] = useState<{ value: string }[]>([])
    const [optionCustomerReveiveds, setCustomerReveiveds] = useState<{ value: string }[]>([])
    const [labelOrderMoney, setLabelOrderMoney] = useState<string>('0đ')
    const setIsLoading = useSetRecoilState(loadingState)
    const router = useRouter()

    const {
        control,
        handleSubmit,
        setValue,
        formState: {errors},
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
        defaultValues: {
            order: {
                paymentType: EnumCreateOrderDtoPaymentType.receive_payment,
                orderMoney: 0,
                cod: 0,
                codFee: 0,
                collectedMoney: 0,
                notCollectedMoney: 0,
                discount: 0,
                cashCollection: 0
            }
        }
    })
    const formStyle = {
        maxWidth: 'none',
    }
    const formItemLayout = {
        labelCol: {span: 8},
        wrapperCol: {span: 16},
    }
    const watchCustomerSendPhone = useWatch({
        control,
        name: 'customerSendPhone',
    })
    const watchCustomerReceivePhone = useWatch({
        control,
        name: 'customerReceivePhone',
    })
    const watchOrderMoney = useWatch({
        control,
        name: 'order.orderMoney',
        defaultValue: 0
    })
    const watchPaymentType = useWatch({
        control,
        name: 'order.paymentType',
        defaultValue: EnumCreateOrderDtoPaymentType.receive_payment
    })
    const watchDiscount = useWatch({
        control,
        name: 'order.discount',
        defaultValue: 0
    })
    const watchCashCollection = useWatch({
        control,
        name: 'order.cashCollection',
        defaultValue: 0
    })
    const watchCod = useWatch({
        control,
        name: 'order.cod',
        defaultValue: 0
    })
    const watchCodFee = useWatch({
        control,
        name: 'order.codFee',
        defaultValue: 0
    })
    const {TextArea} = Input
    const {Title} = Typography
    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Thêm mới đơn hàng')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                OrderService.getOneBase({
                    id: Number(router?.query?.id),
                    join: [
                        'customerSend',
                        'customerReceived',
                        'location',
                    ]
                }),
                LocationService.getManyBase({
                    limit: 1000
                }),
                CustomerService.getManyBase({
                    limit: 10000
                }),
                BranchOfficeService.getManyBase({
                    limit: 1000
                })
            ])
                .then(([responseOrderData, responseLocationData, responseCustomerData, responseBranchOfficeData]) => {
                    setValue('customerSendPhone.value', responseOrderData?.customerSend?.customerPhone)
                    setValue('customerSendName', responseOrderData?.customerSend?.customerName)
                    setValue('customerReceivePhone.value', responseOrderData?.customerReceived?.customerPhone)
                    setValue('customerReceiveName', responseOrderData?.customerReceived?.customerName)
                    setValue('order', {
                        id: responseOrderData.id,
                        locationId: responseOrderData.locationId,
                        customerSendId: responseOrderData.customerSendId,
                        customerReceivedId: responseOrderData.customerReceivedId,
                        branchOfficeSendId: responseOrderData.branchOfficeSendId,
                        branchOfficeReceiveId: responseOrderData.branchOfficeReceiveId,
                        itemName: responseOrderData.itemName,
                        quantity: responseOrderData.quantity,
                        weight: responseOrderData.weight,
                        discount: responseOrderData.discount,
                        orderMoney: responseOrderData.orderMoney,
                        cashCollection: responseOrderData.cashCollection,
                        cod: responseOrderData.cod,
                        codFee: responseOrderData.codFee,
                        collectedMoney: responseOrderData?.collectedMoney,
                        notCollectedMoney: responseOrderData?.notCollectedMoney,
                        note: responseOrderData.note,
                        address: responseOrderData.address,
                        //@ts-ignore
                        paymentType: responseOrderData.paymentType ? responseOrderData.paymentType : EnumOrderEntityPaymentType.receive_payment
                    })
                    setLocations(responseLocationData.data)
                    setCustomers(responseCustomerData.data)
                    setOptionCustomers(responseCustomerData?.data?.map((customerItem) => {
                        return {value: customerItem?.customerPhone}
                    }))
                    setBranchOffices(responseBranchOfficeData?.data)
                    setIsLoading(false)
                })
                .catch((error) => {
                    alertError(error)
                    setIsLoading(false)
                })
        }
    }, [router])
    const getOrderInfoByCustomerSend = (customerSendPhone:string)=>{
        if(customerSendPhone!="" && customerSendPhone != undefined){
            OrderService.getManyBase({
                filter:[`customerSend.customerPhone||$eq||${customerSendPhone}`],
                join: [
                    'customerSend',
                    'customerReceived',
                    'location',
                ],
                limit:10000
            }).then((orderRespone)=>{
                if(orderRespone?.data?.length>0){
                    setValue('order.branchOfficeSendId',orderRespone?.data?.[0]?.branchOfficeSendId)
                    const customers = orderRespone?.data?.map((orderItem) => {
                        return orderItem?.customerReceived.customerPhone
                    })
                    setCustomerReveiveds(customers.reduce(
                        (unique, item) => (unique.includes(item) ? unique : [...unique, item]),
                        [],
                    ).map((customerPhone) => {
                        return {value:customerPhone}
                    }))
                }
                else{

                    setValue('order.branchOfficeSendId',null)
                    setCustomerReveiveds(customers?.map((customerItem) => {
                        return {value:customerItem.customerPhone}
                    }))
                }
            })
        }

    }
    const getOrderInfoByCustomerReceived = (customerReceivedPhone:string)=>{
        if(customerReceivedPhone!="" && customerReceivedPhone != undefined){
            OrderService.getManyBase({
                filter:[`customerReceived.customerPhone||$eq||${customerReceivedPhone}`],
                join: [
                    'customerSend',
                    'customerReceived',
                    'location',
                ],
                limit:10000
            }).then((orderRespone)=>{
                if(orderRespone?.data?.length>0){
                    setValue('order.locationId',orderRespone?.data?.[0]?.locationId)
                    setValue('order.branchOfficeReceiveId',orderRespone?.data?.[0]?.branchOfficeReceiveId)
                }
                else{
                    setValue('order.locationId',null)
                    setValue('order.branchOfficeReceiveId',null)
                }
            })
        }

    }
    useMemo(() => {
        if (watchCustomerSendPhone && watchCustomerSendPhone.value != '') {
            const customerNameFind = customers?.find((customerItem) => customerItem?.customerPhone == watchCustomerSendPhone?.value)?.customerName || ""
            setValue('customerSendName', customerNameFind)
            getOrderInfoByCustomerSend(watchCustomerSendPhone.value)
        }
    }, [watchCustomerSendPhone])
    useMemo(() => {
        if (watchCustomerReceivePhone && watchCustomerReceivePhone.value != '' ) {
            const customerNameFind = customers?.find((customerItem) => customerItem?.customerPhone == watchCustomerReceivePhone?.value)?.customerName || ""
            setValue('customerReceiveName', customerNameFind)
            getOrderInfoByCustomerReceived(watchCustomerReceivePhone.value)
        }
    }, [watchCustomerReceivePhone])
    useMemo(() => {
        let money = 0
        if (watchPaymentType) {
            money = Number(watchOrderMoney) - Number(watchDiscount) + watchCod + watchCodFee
            if (watchPaymentType === EnumCreateOrderDtoPaymentType.receive_payment) {
                setLabelOrderMoney(formatCurrency(money + watchCashCollection))
                setValue('order.collectedMoney', 0)
                setValue('order.notCollectedMoney', money)
            } else {
                setLabelOrderMoney(formatCurrency(watchCashCollection))
                setValue('order.collectedMoney', money)
                setValue('order.notCollectedMoney', 0)
            }
        }
    }, [watchPaymentType, watchOrderMoney, watchDiscount, watchCod, watchCodFee, watchCashCollection])

    const onSubmit: SubmitHandler<Inputs> = (data) => {

        //check khách hàng đã tồn tại hay chưa
        Promise.all([
            data.customerSendPhone.value &&
            data.customerReceivePhone.value &&
            CustomerService.getManyBase({
                filter: [`customerPhone||$eq||${data.customerSendPhone.value}`],
                limit: 0
            }),
            CustomerService.getManyBase({
                filter: [`customerPhone||$eq||${data.customerReceivePhone.value}`],
                limit: 0
            })
        ]).then(([customerSendDataResponse, customerReceiveDataResponse]) => {
            // setValue('order', responseOrderData.)
            setIsLoading(true)
            let customerSendId = 0
            let customerReceivedId = 0

            if (customerSendDataResponse?.data?.[0]?.id) customerSendId = customerSendDataResponse?.data?.[0]?.id
            else {

                CustomerService.createOneBase(
                    {
                        body: {
                            'customerPhone': data.customerSendPhone.value,
                            'customerName': data.customerSendName
                        }
                    }
                ).then((responseCustomerSend) => customerSendId = responseCustomerSend.id)
            }
            if (customerReceiveDataResponse?.data?.[0]?.id) customerReceivedId = customerReceiveDataResponse?.data?.[0]?.id
            else
                CustomerService.createOneBase(
                    {
                        body: {
                            'customerPhone': data.customerReceivePhone.value,
                            'customerName': data.customerReceiveName
                        }
                    }
                ).then((responseCustomerReceive) => customerReceivedId = responseCustomerReceive.id)
             setTimeout(() => {
                    const order: CreateOrderDto = {
                        orderCode: '',
                        locationId: data.order.locationId,
                        customerSendId: customerSendId,
                        customerReceivedId: customerReceivedId,
                        branchOfficeSendId: data.order.branchOfficeSendId,
                        branchOfficeReceiveId: data.order.branchOfficeReceiveId,
                        itemName: data.order.itemName,
                        quantity: data.order.quantity ? data.order.quantity : 0,
                        weight: data.order.weight ? data.order.weight : 0,
                        discount: data.order.discount,
                        cashCollection: data.order.cashCollection ? data.order.cashCollection : 0,
                        orderMoney: data.order.orderMoney,
                        cod: data.order.cod,
                        codFee: data.order.codFee,
                        collectedMoney: data.order.collectedMoney,
                        notCollectedMoney: data.order.notCollectedMoney,
                        note: data.order.note,
                        address: data.order.address,
                        paymentType: data.order.paymentType,
                        createdById: user.id,
                        modifiedById: user.id
                    }
                    // @ts-ignore
                    if (data?.order?.id) {
                        const dataOrderEdit = {
                            ...order,
                            // @ts-ignore
                            id: data?.order?.id,
                            orderCode: data?.order?.orderCode
                        }
                        modifyEntity(OrderService, dataOrderEdit, title, () => {
                            return router.push(`/order`)
                        }).then()
                    } else {
                        modifyEntity(OrderService, order, title, (response) => {
                            return router.push(`/order/${response.id}`)
                        }).then()
                    }
                     setIsLoading(false)
                },
                2000);
        })
        setIsLoading(false)
    }
    return (
        <Content title={title} onBack={() => router.push('/order')}>
            <Form
                {...formItemLayout}
                name="basic"
                style={formStyle}
                onFinish={handleSubmit(onSubmit)} autoComplete={'off'}
            >
                <Card title="Thông tin khách hàng" bordered={false}>
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label={'SĐT Người gửi'}
                                       required={true}
                                       validateStatus={errors?.customerSendPhone?.value && 'error'}
                                       help={errors?.customerSendPhone?.value && errors?.customerSendPhone?.value?.message}
                            >
                                <Controller
                                    name={'customerSendPhone.value'}
                                    control={control}
                                    render={({field}) => (
                                        <AutoComplete
                                            {...field}
                                            options={optionCustomers}
                                            placeholder="Nhập số điện thoại người gửi"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item
                                required={true}
                                label={<span>Tên người gửi </span>}
                                validateStatus={errors.customerSendName && 'error'}
                                help={errors?.customerSendName && errors?.customerSendName?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'customerSendName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            size="middle"
                                            placeholder="Tên người gửi"
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label={'VP gửi'}
                                       required={true}
                                       validateStatus={errors.order?.branchOfficeSendId && 'error'}
                                       help={errors.order?.branchOfficeSendId && errors?.order?.branchOfficeSendId.message}
                            >
                                <Controller
                                    name={'order.branchOfficeSendId'}
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            allowClear
                                            {...field}
                                            placeholder={'Chọn VP gửi'}
                                            className={'w-full'}
                                        >
                                            {branchOffices?.map(
                                                (branchOfficeItem, branchOfficeIndex) => (
                                                    <Select.Option
                                                        value={branchOfficeItem.id}
                                                        key={`op-branchOfficeSend-${branchOfficeIndex}`}
                                                    >
                                                        {branchOfficeItem.branchOfficeName}
                                                    </Select.Option>
                                                )
                                            )}
                                        </Select>
                                    )}
                                />
                            </Form.Item>

                        </Col>
                    </Row>
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label={'SĐT Người nhận'}
                                       required={true}
                                       validateStatus={errors?.customerReceivePhone?.value && 'error'}
                                       help={errors?.customerReceivePhone?.value && errors?.customerReceivePhone?.value?.message}
                            >
                                <Controller
                                    name={'customerReceivePhone.value'}
                                    control={control}
                                    render={({field}) => (
                                        <AutoComplete
                                            {...field}
                                            options={optionCustomerReveiveds}
                                            placeholder="Nhập số điện thoại người nhận"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item
                                required={true}
                                label={<span>Tên người nhận </span>}
                                validateStatus={errors.customerReceiveName && 'error'}
                                help={errors?.customerReceiveName && errors?.customerReceiveName?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'customerReceiveName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            size="middle"
                                            placeholder="Tên người nhận"
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label={'VP nhận'}
                                       required={true}
                                       validateStatus={errors.order?.branchOfficeReceiveId && 'error'}
                                       help={errors.order?.branchOfficeReceiveId && errors?.order?.branchOfficeReceiveId.message}
                            >
                                <Controller
                                    name={'order.branchOfficeReceiveId'}
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            allowClear
                                            {...field}
                                            placeholder={'Chọn VP nhận'}
                                            className={'w-full'}
                                        >
                                            {branchOffices?.map(
                                                (branchOfficeItem, branchOfficeIndex) => (
                                                    <Select.Option
                                                        value={branchOfficeItem.id}
                                                        key={`op-branchOfficeReceive-${branchOfficeIndex}`}
                                                    >
                                                        {branchOfficeItem.branchOfficeName}
                                                    </Select.Option>
                                                )
                                            )}
                                        </Select>
                                    )}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item
                                // required={true}
                                label={<span>Địa chỉ giao hàng</span>}
                                // validateStatus={errors?.order?.address && 'error'}
                                // help={errors?.order?.address && errors?.order?.address?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'order.address'}
                                    render={({field}) => (
                                        <TextArea
                                            {...field}
                                            size="middle"
                                            placeholder="Địa chỉ giao hàng"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label={'Điểm giao'}
                                       required={true}
                                       validateStatus={errors.order?.locationId && 'error'}
                                       help={errors.order?.locationId && errors?.order?.locationId.message}
                            >
                                <Controller
                                    name={'order.locationId'}
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            allowClear
                                            {...field}
                                            placeholder={'Chọn điểm giao'}
                                            className={'w-full'}
                                        >
                                            {locations?.map(
                                                (locationItem, locationIndex) => (
                                                    <Select.Option
                                                        value={locationItem.id}
                                                        key={`op-location-${locationIndex}`}
                                                    >
                                                        {locationItem.locationName}
                                                    </Select.Option>
                                                )
                                            )}
                                        </Select>
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Ghi chú">
                                <Controller
                                    control={control}
                                    name={'order.note'}
                                    render={({field}) => (
                                        <TextArea
                                            {...field}
                                            placeholder="Nhập ghi chú"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                    </Row>


                </Card>

                <Card title="Hàng hóa" bordered={false}>
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item
                                label={<span>Tên hàng hóa</span>}
                                required={true}
                                validateStatus={errors.order?.itemName && 'error'}
                                help={errors.order?.itemName && errors.order?.itemName?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'order.itemName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            size="middle"
                                            placeholder="Nhập tên hàng hóa"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Số lượng"
                                // required={true}
                                // validateStatus={errors.order?.quantity && 'error'}
                                // help={errors.order?.quantity && errors.order?.quantity?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'order.quantity'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            min={0}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Trọng lượng"
                                       validateStatus={errors.order?.weight && 'error'}
                                       help={errors.order?.weight && errors.order?.weight?.message}>
                                <Controller
                                    control={control}
                                    name={'order.weight'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            addonAfter={'KG'}
                                            min={0}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                </Card>
                <Card title="Cước phí" bordered={false}
                >
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label={'Hình thức thanh toán'}>
                                <Controller
                                    name={'order.paymentType'}
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            showSearch
                                            placeholder="Vui lòng chọn hình thức thanh toán"
                                            size={'middle'}
                                            {...field}
                                        >
                                            <Select.Option value={EnumOrderEntityPaymentType.in_office}>
                                                Tại văn phòng
                                            </Select.Option>
                                            <Select.Option value={EnumOrderEntityPaymentType.receive_payment}>
                                                Nhận hàng thanh toán
                                            </Select.Option>
                                        </Select>
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Thu hộ">
                                <Controller
                                    control={control}
                                    name={'order.cashCollection'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            placeholder="Thu hộ"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                    </Row>
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Giá cước"
                                       required={true}
                                       validateStatus={errors.order?.orderMoney && 'error'}
                                       help={errors.order?.orderMoney && errors.order?.orderMoney?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'order.orderMoney'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            placeholder="Giá cước"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="COD">
                                <Controller
                                    control={control}
                                    name={'order.cod'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            placeholder="COD"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Phí COD">
                                <Controller
                                    control={control}
                                    name={'order.codFee'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            placeholder="Phí COD"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                    </Row>
                    <Row className={'w-full'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Đã thu">
                                <Controller
                                    control={control}
                                    name={'order.collectedMoney'}
                                    render={({field}) => (
                                        <InputNumber
                                            disabled={true}
                                            {...field}
                                            placeholder="Đã thu"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>

                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Chưa thu">
                                <Controller
                                    control={control}
                                    name={'order.notCollectedMoney'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            disabled={true}
                                            placeholder="Chưa thu"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>


                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Form.Item label="Giảm giá">
                                <Controller
                                    control={control}
                                    name={'order.discount'}
                                    render={({field}) => (
                                        <InputNumber
                                            {...field}
                                            placeholder="Giảm giá"
                                            addonAfter={'đ'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                    </Row>
                    <Row className={'w-full text-right'} gutter={[16, 16]}>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Title level={3}>COD: {formatCurrency(watchCod)}</Title>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Title level={3}>Phí COD: {formatCurrency(watchCodFee)}</Title>
                        </Col>
                        <Col xs={24} sm={24} md={8} lg={8} xl={8}>
                            <Title level={3}>Phải thu: {labelOrderMoney} </Title>
                        </Col>
                    </Row>
                </Card>
                <Row className={'float-right'}>
                    {router.query?.id!="create" && (
                        <Tooltip title={'In'}>
                            <a href={`/order/print/${router.query?.id}`} target={"_blank"}>
                                <Button type="primary" size={'large'}
                                        icon={<PrinterOutlined />}
                                >In đơn hàng</Button>
                            </a>
                        </Tooltip>
                    )

                    }
                    <Button type="primary" size={'large'} htmlType="submit" className={"ml-2"} style={{width: '200px'}}>
                        Lưu lại
                    </Button>

                </Row>
            </Form>
        </Content>
    )
}
export default EditOrder