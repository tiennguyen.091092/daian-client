import React, {useEffect, useState} from 'react'
import {authState, countDataTable} from 'recoil/Atoms'
import {useRecoilValue} from 'recoil'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {formatCurrency, formatDate, formatDateDDMMYYYY, getStatusOrder} from 'utils'
import {ColumnsType} from 'antd/es/table'
import {EnumOrderEntityStatus, OrderEntity, OrderService, RouteEntity, RouteService} from 'services'
import {Button, Card, Col, Form, Input, Row, Select, Space, Tag, Typography} from 'antd'
import DataTable from 'common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import FormItem from 'common/FormItem'
import Link from 'common/Link'
import {endOfDay, startOfDay} from 'date-fns'
import DatePicker from 'common/DatePicker'
import CreateOnOrder from 'pages/on-order/create'
import {NextPage} from 'next'
import {DownloadOutlined} from "@ant-design/icons";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';

interface Inputs {
    q?: string
    type?: string
    dates?: [Date, Date]
    orderStatus?: EnumOrderEntityStatus | string
    driveCarSearch?: string
    routeId?: string
}

const {Title} = Typography

const Index: NextPage = () => {
    const [title] = useState<string>('Đơn gửi hàng')
    const user = useRecoilValue(authState)
    const [selectedOrders, setSelectedOrders] = useState<OrderEntity[]>([])
    const [routes, setRoutes] = useState<RouteEntity[]>([])
    const [isShowModal, setIsShowModal] = useState(false)
    const [totalOrderMoney, setTotalOrderMoney] = useState(0)
    const [totalCollectedMoney, setCollectedMoney] = useState(0)
    const [totalNotCollectedMoney, setTotalNotCollectedMoney] = useState(0)
    const [totalCashCollection, setTotalCashCollection] = useState(0)
    const countDatatable = useRecoilValue(countDataTable)
    const {startOfWeek, endOfWeek} = getStartAndEndOfWeek();
    const {control, handleSubmit, setValue} = useForm<Inputs>({
        defaultValues: {
            q: '',
            type: 'orderCode',
            dates: [startOfWeek, endOfWeek],
            orderStatus: 'all',
            driveCarSearch: '',
            routeId: "0"
        },
    })
    const {RangePicker} = DatePicker

    const defaultFilter: any = [`createdAt||between||${startOfDay(startOfWeek).toISOString()},${endOfDay(endOfWeek).toISOString()}`]
    useEffect(() => {
        if (user.roleId != 1) {
            defaultFilter.push(`createdById||$eq||${user.id}`)
            setTableServiceParams(prevState => {
                return {
                    ...prevState,
                    filter: defaultFilter
                }
            })
        }

    }, [user])
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
        filter: defaultFilter,
        join: [
            'createdBy',
            'customerSend',
            'branchOfficeSend',
            'customerReceived',
            'branchOfficeReceive',
            'location',
            'onOrder',
            'onOrder.route',
            'onOrder.driverCar'
        ],
    })
    const columns: ColumnsType<OrderEntity> = [
        {
            dataIndex: 'onOrder',
            title: 'Chuyến',
            render: (value, record) => {
                return (<>
                        <b>{record?.onOrder?.route?.name}</b>
                        <br/>
                        <span>{record?.onOrder?.time}</span> <span>{formatDateDDMMYYYY(record?.onOrder?.date)}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'driverCar',
            title: 'Xe - Tài xế',
            render: (value, record) => {
                return (<>
                        <b>{record?.onOrder?.driverCar?.driverName}</b>
                        <br/>
                        <span>{record?.onOrder?.driverCar?.licensePlates}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'location',
            title: 'Điểm giao',
            render: (value, record) => record?.location?.locationName,
        },
        {
            dataIndex: 'customerSend',
            title: 'Người gửi',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerSend?.customerName}</b>
                        <br/>
                        <span>{record?.customerSend?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'customerReceived',
            title: 'Người nhận',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerReceived?.customerName}</b>
                        <br/>
                        <span>{record?.customerReceived?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'itemName',
            title: 'Tên hàng hóa',
        },
        {
            dataIndex: 'quantity',
            title: 'Số lượng',
            align: 'center',
        },
        {
            dataIndex: 'orderMoney',
            title: 'Cước',
            align: 'right',
            render: (value) => formatCurrency(value),
        },
        {
            dataIndex: 'collectedMoney',
            title: 'Đã thu',
            align: 'right',
            render: (value) => formatCurrency(value),
        },
        {
            dataIndex: 'notCollectedMoney',
            title: 'Chưa thu',
            align: 'right',
            render: (value) => formatCurrency(value),
        },
        {
            dataIndex: 'cashCollection',
            title: 'Thu hộ',
            align: 'right',
            render: (value) => formatCurrency(value),
        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align: 'center',
            render: (value) => formatDate(value),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            render: (value) =>
                value === EnumOrderEntityStatus.created ? (
                    <Tag color={'success'}>{getStatusOrder(value)}</Tag>
                ) : value === EnumOrderEntityStatus.on_order ? (
                    <Tag color="#108ee9">{getStatusOrder(value)}</Tag>
                ) : value === EnumOrderEntityStatus.off_order ? (
                    <Tag color={'error'}>{getStatusOrder(value)}</Tag>
                ) : null,
        },

    ]

    function getStartAndEndOfWeek() {
        const today = new Date();
        const dayOfWeek = today.getDay(); // Ngày trong tuần (0: Chủ Nhật, 1: Thứ Hai, ..., 6: Thứ Bảy)
        const startOfWeek = new Date(today); // Khởi tạo một đối tượng Date mới với cùng ngày và thời gian
        const endOfWeek = new Date(today); // Khởi tạo một đối tượng Date mới với cùng ngày và thời gian

        // Điều chỉnh đối tượng Date để lấy ngày đầu tuần (Thứ Hai)
        startOfWeek.setDate(today.getDate() - dayOfWeek + (dayOfWeek === 0 ? -6 : 1));

        // Điều chỉnh đối tượng Date để lấy ngày cuối tuần (Chủ Nhật)
        endOfWeek.setDate(today.getDate() - dayOfWeek + 7);

        return {startOfWeek, endOfWeek};
    }

    const removeFilter = (filters: string[], key) => {
        let existIndex = -1
        if (filters.length > 0) {
            filters.map((filter, index) => {
                if (filter.includes(key)) {
                    existIndex = index
                }
            })
            if (existIndex > -1) filters.splice(existIndex, 1)
        }
        return filters
    }
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        setTableServiceParams((prevState) => {
            if (data.q) {
                let strFilter = ''
                let strOr = ''
                switch (data.type) {
                    case 'orderCode':
                        strFilter = `orderCode||$eq||${data?.q}`
                        removeFilter(prevState.filter, 'customerName')
                        removeFilter(prevState.filter, 'customerPhone')
                        removeFilter(prevState.filter, '')
                        break
                    case 'customerName':
                        strFilter = `customerSend.customerName||$eq||${data?.q}`
                        strOr = `customerReceived.customerName||$eq||${data?.q}`
                        removeFilter(prevState.filter, 'code')
                        removeFilter(prevState.filter, 'customerPhone')
                        removeFilter(prevState.filter, '')
                        break
                    case 'customerPhone':
                        strFilter = `customerSend.customerPhone||$eq||${data?.q}`
                        strOr = `customerReceived.customerPhone||$eq||${data?.q}`
                        removeFilter(prevState.filter, 'code')
                        removeFilter(prevState.filter, 'customerName')
                        removeFilter(prevState.filter, '')
                        break
                }

                    prevState = {
                        ...prevState,
                        filter: [...removeFilter(prevState.filter, data.type), strFilter],
                        or: [...removeFilter(prevState.filter, data.type), strOr],}

            } else {
                removeFilter(prevState.filter, 'orderCode')
                removeFilter(prevState.filter, 'customerName')
                removeFilter(prevState.filter, 'customerPhone')
                removeFilter(prevState.filter, '')
                prevState.or = []
            }

            if (data?.dates?.length > 0) {
                prevState = {
                    ...prevState,
                    filter: [
                        ...removeFilter(prevState.filter, 'createdAt'),
                        `createdAt||between||${startOfDay(
                            data?.dates[0]
                        ).toISOString()},${endOfDay(data?.dates[1]).toISOString()}`
                    ],
                }
            } else {
                removeFilter(prevState.filter, 'createdAt')
            }
            if (data?.orderStatus != 'all') {
                prevState = {
                    ...prevState,
                    filter: [
                        ...removeFilter(prevState.filter, 'status'),
                        `status||$eq||${data.orderStatus}`,

                    ],
                }
            } else {
                removeFilter(prevState.filter, 'status')
            }
            if (data?.driveCarSearch != '') {
                prevState = {
                    ...prevState,
                    filter: [
                        ...removeFilter(prevState.filter, 'driveCarSearch'),
                        `onOrder.driverCar.driverName||$eq||${data.driveCarSearch}`,
                    ],
                    or: [...removeFilter(prevState.filter, data.type), `onOrder.driverCar.licensePlates||$eq||${data.driveCarSearch}`],

                }
            } else
                removeFilter(prevState.filter, 'driveCarSearch')

            if (data?.routeId != '' && data?.routeId != '0') {
                prevState = {
                    ...prevState,
                    filter: [
                        ...removeFilter(prevState.filter, 'routeId'),
                        `onOrder.route.id||$eq||${Number(data.routeId)}`,
                    ],
                }
            } else
                removeFilter(prevState.filter, 'routeId')

            if (user.roleId != 1) {
                prevState.filter.push(`createdById||$eq||${user.id}`)
            }
            return {...prevState}
        })
    }
    const onReset = () => {
        setValue('q', '')
        setValue('type', 'orderCode')
        //@ts-ignore
        setValue('dates', [])
        setValue('orderStatus', 'all')
        setTableServiceParams({
            sort: ['createdAt,DESC'],
            filter: defaultFilter,
            join: [
                'createdBy',
                'customerSend',
                'branchOfficeSend',
                'customerReceived',
                'branchOfficeReceive',
                'location',
                'onOrder',
                'onOrder.route',
                'onOrder.driverCar'
            ],
        })
    }
    useEffect(() => {
        OrderService.getManyBase({...tableServiceParams, limit: 100000}).then((response) => {
            setTotalOrderMoney(response.data.reduce(function (accumulator, curValue) {
                return accumulator + curValue.orderMoney
            }, 0))
            setCollectedMoney(response.data.reduce(function (accumulator, curValue) {
                return accumulator + curValue.collectedMoney
            }, 0))
            setTotalNotCollectedMoney(response.data.reduce(function (accumulator, curValue) {
                return accumulator + curValue.notCollectedMoney
            }, 0))
            setTotalCashCollection(response.data.reduce(function (accumulator, curValue) {
                return accumulator + curValue.cashCollection
            }, 0))
        })
    }, [tableServiceParams])
    useEffect(() => {
        RouteService.getManyBase().then((routerResponse) => {
            setRoutes(routerResponse.data)
        })
    }, [])

    const exportToCSV = () => {
        const fileType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
        const fileExtension = '.xlsx';
        const fileName = `${title} - ${formatDate(
            new Date(),
        )}`
        tableServiceParams.join = [
            'createdBy',
            'customerSend',
            'branchOfficeSend',
            'customerReceived',
            'branchOfficeReceive',
            'location',
            'onOrder',
            'onOrder.route',
            'onOrder.driverCar'
        ]
        OrderService.getManyBase({...tableServiceParams, limit: 100000}).then((response) => {
            const dataExport = response.data?.map((orderItem) => {
                const routerName = orderItem.onOrder?.route?.name && orderItem.onOrder?.route?.name != undefined ? orderItem.onOrder?.route?.name : ""
                const orderTime = orderItem?.onOrder?.time && orderItem?.onOrder?.time != undefined ? orderItem?.onOrder?.time : ""
                const driverName = orderItem.onOrder?.driverCar?.driverName && orderItem.onOrder?.driverCar?.driverName != undefined ? orderItem.onOrder?.driverCar?.driverName : ""
                const licensePlates = orderItem.onOrder?.driverCar?.licensePlates && orderItem.onOrder?.driverCar?.licensePlates != undefined ? orderItem.onOrder?.driverCar?.licensePlates : ""
                return {
                    "Chuyến": `${routerName} \n ${orderTime} ${formatDateDDMMYYYY(orderItem?.onOrder?.date)} `,
                    "Xe - Tài xế": `${driverName} \n ${licensePlates}`,
                    "Điểm giao": orderItem?.location?.locationName,
                    "Người gửi": orderItem?.customerSend?.customerName,
                    "SĐT Người gửi": orderItem?.customerSend?.customerPhone,
                    "Người nhận": orderItem?.customerReceived?.customerName,
                    "SĐT Người nhận": orderItem?.customerReceived?.customerPhone,
                    "Tên hàng hóa": orderItem?.itemName,
                    "Số lượng": orderItem?.quantity,
                    "Cước": orderItem?.orderMoney,
                    "Đã thu": orderItem?.collectedMoney,
                    "Chưa thu": orderItem?.notCollectedMoney,
                    "Thu hộ": orderItem?.cashCollection,
                    "Ngày tạo": formatDate(orderItem?.createdAt),
                }
            })
            const ws = XLSX.utils.json_to_sheet(dataExport);
            const wb = {Sheets: {'data': ws}, SheetNames: ['data']};
            const excelBuffer = XLSX.write(wb, {bookType: 'xlsx', type: 'array'});
            const data = new Blob([excelBuffer], {type: fileType});
            FileSaver.saveAs(data, fileName + fileExtension);
        })

    }
    return (
        <React.Fragment>
            <Content
                title={title}
            >
                <Row gutter={16}>
                    <Col span={6}>
                        <Card bordered={false}>
                            <Title level={4} className={'m-0 ml-2'}>TỔNG TIỀN CƯỚC</Title>
                            <Title level={2}
                                   className={'m-0 ml-2 text-blue-900'}>{formatCurrency(totalOrderMoney)}</Title>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <Title level={4} className={'m-0 ml-2'}>TỔNG TIỀN ĐÃ THU</Title>
                            <Title level={2}
                                   className={'m-0 ml-2 text-emerald-500'}>{formatCurrency(totalCollectedMoney)}</Title>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <Title level={4} className={'m-0 ml-2'}>THU HỘ</Title>
                            <Title level={2}
                                   className={'m-0 ml-2 text-red-500'}>{formatCurrency(totalCashCollection)}</Title>
                        </Card>
                    </Col>
                    <Col span={6}>
                        <Card bordered={false}>
                            <Title level={4} className={'m-0 ml-2'}>TỔNG TIỀN CHƯA THU</Title>
                            <Title level={2}
                                   className={'m-0 ml-2 text-red-500'}>{formatCurrency(totalNotCollectedMoney)}</Title>
                        </Card>
                    </Col>
                </Row>
                <Card>
                    <Form
                        autoComplete={'off'}
                        onFinish={handleSubmit(onSubmit)}
                        onReset={onReset}
                    >
                        <Row gutter={[16, 16]}>
                            <Col xs={24} sm={24} md={8} xl={8} lg={8}>
                                <FormItem>
                                    <Input.Group compact>
                                        <Controller
                                            control={control}
                                            name={'type'}
                                            render={({field}) => (
                                                <Select
                                                    {...field}
                                                    placeholder="Tìm kiếm theo"
                                                    style={{width: '50%'}}
                                                    defaultValue="id"
                                                >
                                                    <Select.Option value="orderCode">Mã đơn hàng</Select.Option>
                                                    <Select.Option value="customerName">
                                                        Tên người gửi/nhận
                                                    </Select.Option>
                                                    <Select.Option value="customerPhone">
                                                        SĐT người gửi/nhận
                                                    </Select.Option>
                                                </Select>
                                            )}
                                        />
                                        <Controller
                                            control={control}
                                            name={'q'}
                                            render={({field}) => (
                                                <Input
                                                    {...field}
                                                    style={{width: '50%'}}
                                                    placeholder={`Nhập vào`}
                                                />
                                            )}
                                        />
                                    </Input.Group>
                                </FormItem>
                            </Col>
                            <Col xs={24} sm={24} md={6} xl={6} lg={6}>
                                <Form.Item label={'Từ ngày'}>
                                    <Controller
                                        control={control}
                                        name={'dates'}
                                        render={({field}) => (
                                            //@ts-ignore
                                            <RangePicker
                                                format={'dd/MM/YYYY'}
                                                picker="date"
                                                {...field}
                                                style={{
                                                    width: '100%',
                                                }}
                                            />
                                        )}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={6} xl={6} lg={6}>
                                <Form.Item label={"Xe -Tài xế"}>
                                    <Controller
                                        control={control}
                                        name={'driveCarSearch'}
                                        render={({field}) => (
                                            <Input
                                                {...field}
                                                placeholder={`Nhập vào tên tài xế hoặc biển số xe`}
                                            />
                                        )}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} sm={24} md={4} xl={4} lg={4} className={'text-right'}>
                                <FormItem>
                                    <Space>
                                        <Button htmlType={'reset'}>Đặt lại</Button>
                                        <Button type={'primary'} htmlType={'submit'}>
                                            Tìm kiếm
                                        </Button>
                                    </Space>
                                </FormItem>
                            </Col>
                            <Col xs={24} sm={24} md={8} xl={8} lg={8}>
                                <Form.Item label={'Chuyến xe'}>
                                    <Controller
                                        control={control}
                                        name={'routeId'}
                                        render={({field}) => (
                                            <Select
                                                showSearch
                                                placeholder="Chọn Chuyến xe"
                                                size={'middle'}
                                                {...field}
                                            >
                                                <Select.Option value='0'>
                                                    Tất cả chuyến xe
                                                </Select.Option>
                                                {routes?.length > 0 && routes?.map((routeItem) => {
                                                    return (
                                                        <Select.Option value={routeItem.id}
                                                                       key={`routeSelectItem-${routeItem.id}`}>
                                                            {routeItem.name}
                                                        </Select.Option>
                                                    )
                                                })
                                                }
                                            </Select>
                                        )}
                                    />
                                </Form.Item>
                            </Col>

                            <Col xs={24} sm={24} md={8} xl={8} lg={8}>
                                <Form.Item label={'Trạng thái ĐH'}>
                                    <Controller
                                        control={control}
                                        name={'orderStatus'}
                                        render={({field}) => (
                                            <Select
                                                showSearch
                                                placeholder="Chọn trạng thái"
                                                size={'middle'}
                                                {...field}
                                            >
                                                <Select.Option value='all'>
                                                    Tất cả trạng thái
                                                </Select.Option>
                                                <Select.Option value={EnumOrderEntityStatus.created}>
                                                    {getStatusOrder(EnumOrderEntityStatus.created)}
                                                </Select.Option>
                                                <Select.Option value={EnumOrderEntityStatus.on_order}>
                                                    {getStatusOrder(EnumOrderEntityStatus.on_order)}
                                                </Select.Option>
                                                <Select.Option value={EnumOrderEntityStatus.off_order}>
                                                    {getStatusOrder(EnumOrderEntityStatus.off_order)}
                                                </Select.Option>
                                            </Select>
                                        )}
                                    />
                                </Form.Item>
                            </Col>

                        </Row>
                    </Form>
                </Card>
                <Card
                    title={
                        <span className={'text-2xl'}>{countDatatable} Đơn gửi hàng</span>
                    }
                    extra={
                        <React.Fragment>

                            <Link href={'/order/create'}>
                                <Button
                                    type={'primary'}>
                                    Thêm mới Đơn gửi hàng
                                </Button>
                            </Link>
                            <Button type={'primary'} danger style={{marginLeft: '5px'}}
                                    disabled={!(selectedOrders?.length > 0)}
                                    onClick={() => {
                                        setIsShowModal(true)
                                    }}>
                                Lên hàng
                            </Button>
                        </React.Fragment>
                    }
                >
                    <Button
                        type={'primary'}
                        className={'float-right mb-2'}
                        onClick={exportToCSV}
                    >
                        <DownloadOutlined/> Tải Excel
                    </Button>
                    <DataTable
                        service={OrderService.getManyBase}
                        serviceParams={tableServiceParams}
                        deleteService={OrderService.deleteOneBase}
                        columns={columns}
                        action={['print', 'edit']}
                        path={'/order'}
                        rowSelection={{
                            type: 'checkbox',
                            onChange: (
                                selectedRowKeys: React.Key[],
                                selectedRows: OrderEntity[]
                            ) => setSelectedOrders(selectedRows),
                            selectedRowKeys: selectedOrders.map(({id}) => id),
                            //@ts-ignore
                            getCheckboxProps: (record: OrderEntity) => ({
                                disabled: record.status === EnumOrderEntityStatus.on_order, // Column configuration not to be checked
                                id: record.id,
                            }),
                        }}
                    />
                </Card>
            </Content>
            {selectedOrders?.length > 0 &&
                <CreateOnOrder isShowModal={isShowModal} setIsShowModal={setIsShowModal}
                               selectedOrders={selectedOrders}/>
            }

        </React.Fragment>
    )
}

export default Index