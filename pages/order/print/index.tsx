import * as React from 'react'
import {EnumOrderEntityPaymentType, OrderEntity,} from 'services'
import {Col, Row, Typography} from 'antd'
import {formatCurrency, formatDate} from 'utils'

interface IProps {
    order: OrderEntity| undefined
    companyTitle:string | string []
}

type TState = {

}
const {Title, Text} = Typography

export default class ComponentToPrint extends React.Component<IProps, TState> {
    constructor(props) {
        super(props)
        this.state = {}

    }
    canvasEl
    componentDidMount() {
    }
    setRef = (ref) => (this.canvasEl = ref)
    render() {
        const {order,companyTitle= "Đại An Logistics"} = this.props
        return (
            <>
                <Row gutter={16} className={'ml-1 mr-1 p-5'}>
                    <Col span={6} className={'text-center'}></Col>
                    <Col span={12} className={'text-center m-0'}><Title level={3} className={'m-0'}>{companyTitle}</Title></Col>
                    <Col span={6} className={'text-center m-0'}> <Title level={4} className={'m-0'}>{order?.orderCode}</Title></Col>
                    <Col span={24} className={'text-center m-0'}><Title level={3} className={'m-0'}>Hotline: 0932329955</Title></Col>
                    <Col span={6} className={'text-center'}></Col>
                    <Col span={12} className={'text-center'}>
                        <Title level={5}>Mỹ Đình - Lạng Sơn - Hữu Nghị - Đồng Đăng - Tân Thanh <br/> Thất Khê - Văn Quan
                            - Lộc Bình - Đình Lập</Title>
                    </Col>
                    <Col span={6} className={'text-center'}></Col>
                    <Col span={6} className={'text-center'}></Col>
                    <Col span={12} className={'text-center'}><Title level={2} className={'mb-2'}>PHIẾU GỬI HÀNG</Title></Col>
                    <Col span={6} className={'text-center'}></Col>

                    <Col span={12} className={'text-left gutter-row'}>
                        <div className={'border border-black'}>
                            <Title level={2} className={'m-0 ml-2'}>VP gửi: {order?.branchOfficeSend?.branchOfficeName}</Title>
                            <Title level={5} className={'m-0 ml-2'}>{order?.branchOfficeSend?.branchOfficeAddress?order?.branchOfficeSend?.branchOfficeAddress:<div>&nbsp;</div>}</Title>
                            <Title level={5} className={'m-0 ml-2'}>{order?.branchOfficeSend?.branchOfficePhone?order?.branchOfficeSend?.branchOfficePhone:<div>&nbsp;</div>}</Title>
                        </div>
                    </Col>
                    <Col span={12} className={'text-left gutter-row'}>
                        <div className={'border border-black  pl-2'}>
                            <Title level={2} className={'m-0 ml-2'}>VP nhận: {order?.branchOfficeReceive?.branchOfficeName}</Title>
                            <Title level={5} className={'m-0 ml-2'}>{order?.branchOfficeReceive?.branchOfficeAddress?order?.branchOfficeReceive?.branchOfficeAddress:<div>&nbsp;</div>}</Title>
                            <Title level={5} className={'m-0 ml-2'}>{order?.branchOfficeReceive?.branchOfficePhone?order?.branchOfficeReceive?.branchOfficePhone:<div>&nbsp;</div>}</Title>
                        </div>
                    </Col>
                    <Col span={12} className={'text-left  mt-2'}>
                        <Title level={5} className={'m-0'}>Người gửi: {order?.customerSend?.customerName}</Title>
                        <Title level={5} className={'m-0'}>Số ĐT: {order?.customerSend?.customerPhone}</Title>
                        <Title level={5} className={'m-0'}>Địa chỉ: {order?.customerSend?.customerAddress}</Title>

                    </Col>
                    <Col span={12} className={'text-left mt-2'}>
                        <div className={'pl-2'}>
                            <Title level={5} className={'m-0'}>Người nhận: {order?.customerReceived?.customerName}</Title>
                            <Title level={5} className={'m-0'}>Số ĐT: {order?.customerReceived?.customerPhone}</Title>
                            <Title level={5} className={'m-0'}>Địa chỉ: {order?.address}</Title>
                        </div>
                    </Col>
                    <Col span={24} className={'space-y-1 mt-2'}>
                        <table className={'w-full border border-black'}>
                            <tr className={'border border-black bg-slate-300'}>
                                <th className={'w-1/12 border border-black'}>STT</th>
                                <th className={'w-1/5 border border-black'}>Tên hàng</th>
                                <th className={'w-1/5 border border-black'}>Số lượng</th>
                                <th className={'w-1/5 border border-black'}>KG</th>
                                <th className={'w-1/5 border border-black'}>Thu hộ (VNĐ)</th>
                                <th className={'w-1/5 border border-black'}>Cước (VNĐ)</th>
                            </tr>
                            <tr className={'border border-black'}>
                                <td className={'border border-black text-center'}>1</td>
                                <td className={'border border-black text-center'}>{order?.itemName}</td>
                                <td className={'border border-black text-center'}>{order?.quantity}</td>
                                <td className={'border border-black text-center'}>{order?.weight}</td>
                                <td className={'border border-black text-center'}>{formatCurrency(order?.cashCollection)}</td>
                                <td className={'border border-black text-center'}>{formatCurrency(order?.orderMoney)}</td>
                            </tr>

                        </table>

                    </Col>
                    <Row className={'w-full p-2'}>
                        <Col span={6} className={'text-left'}>
                            <Title level={3} className={'m-0'}>Ghi chú: {order?.note} </Title>
                        </Col>
                        <Col span={6} className={'text-center'}>
                            <Title level={3} className={'m-0'}>COD: {formatCurrency(order?.cod)}</Title>
                        </Col>
                        <Col span={6} className={'text-center'}>
                            <Title level={3} className={'m-0'}>Phí COD: {formatCurrency(order?.codFee)}</Title>
                        </Col>
                        <Col span={6} className={'text-right'}>
                            <Title level={3} className={'m-0'}> Phải thu: {order?.paymentType===EnumOrderEntityPaymentType.in_office?(formatCurrency(0+order?.cashCollection)):(formatCurrency(order?.orderMoney+order?.cod+order?.codFee+order?.cashCollection))}</Title>
                        </Col>
                    </Row>
                    <Col span={2} className={'text-right w-full'}>
                        <b>Lưu ý:</b>
                    </Col>
                    <Col span={22} className={'text-left'}>
                        <Text className={'m-0'}> - Dịch vụ chuyển hàng tận nơi theo yêu cầu - Giá cước thấp nhất</Text>
                    </Col>
                    <Col span={2} className={'text-right'}>
                        <b></b>
                    </Col>
                    <Col span={22} className={'text-left'}>
                        <Text className={'m-0'}> - Bồi thường tối đa 10 lần giá trị cước thanh toán - Khiếu nại trong 3
                            ngày, sau 3 ngày nhà xe không xử lý</Text>
                    </Col>
                    <Col span={2} className={'text-right'}>
                        <b></b>
                    </Col>
                    <Col span={22} className={'text-left'}>
                        <Text className={'m-0'}> - Khuyến mại 20% cho khách hàng thân thiết và khách hàng gửi 2
                            chiều</Text>
                    </Col>
                    <Col span={2} className={'text-right'}>
                        <b></b>
                    </Col>
                    <Col span={22} className={'text-left'}>
                        <Text className={'m-0'}> - Người gửi hàng chịu trách nhiệm về mọi thông tin khai báo trên phiếu gửi đơn hàng trước pháp luật Việt Nam </Text>
                    </Col>
                    <Row className={'mt-2 w-full'}>
                        <Col span={8} className={'text-center'}>
                            <b className={'m-0'}>Người gửi hàng xác nhận</b><br/>
                            <Text>Tôi đã đọc và đồng ý các nội dung trên</Text>
                        </Col>
                        <Col span={8} className={'text-center'}>
                            <b className={'m-0'}>Người nhận hàng xác nhận</b><br/>
                            <Text>Tôi đã đọc và đồng ý các nội dung trên</Text>
                        </Col>
                        <Col span={8} className={'text-center'}>
                            <b className={'m-0'}>{formatDate(order?.createdAt)}</b><br/>
                            <Text>NV tạo đơn hàng</Text>
                        </Col>
                    </Row>

                    <Row className={'mt-10 w-full'}>
                        <Col span={16} className={'text-left p-2'}>
                            <Text>Đại An Logistics ©2023</Text>
                        </Col>
                        <Col span={8} className={'text-center'}>
                            <Text>{order?.createdBy?.fullName}</Text>
                        </Col>
                    </Row>
                </Row>
            </>
        )
    }
}

