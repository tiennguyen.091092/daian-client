import {FC, useEffect, useRef, useState} from 'react'
import {useReactToPrint} from 'react-to-print'
import {OrderEntity, OrderService} from 'services'
import {useRouter} from 'next/router'
import ComponentToPrint from 'pages/order/print/index'

const Print:FC = ()=>{
    // const [shippingSlips,setShippingSlips] = useState<ShippingSlipEntity[]>()
    const [order,setOrder] = useState<OrderEntity>()
    const router = useRouter()
    const handlePrint = useReactToPrint({
        // @ts-ignore
        content: () => invoicePrintRef.current,
    })
    useEffect(()=>{
       handlePrint()
    },[order])
    useEffect(()=>{
        if (router.query?.id) {
            OrderService.getOneBase({
                id: Number(router?.query?.id),
                join: [
                    'customerSend',
                    'customerReceived',
                    'location',
                    'branchOfficeSend',
                    'branchOfficeReceive',
                    'createdBy'
                ]
            }).then((response) =>
                setOrder(response)
            )
        }
    },[router.query?.id])
    const invoicePrintRef = useRef()
    return (<ComponentToPrint companyTitle = {router?.query?.text} ref={invoicePrintRef} order={order} />)
}
export default Print
