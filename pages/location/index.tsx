import React, {FC, useEffect, useState} from 'react'
import {countDataTable} from 'recoil/Atoms'
import {useRecoilValue} from 'recoil'
import {Controller, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {formatDate} from 'utils'
import {ColumnsType} from 'antd/es/table'
import {LocationEntity, LocationService} from 'services'
import {Button, Card, Col, Form, Input, Row, Space} from 'antd'
import DataTable from 'common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import FormItem from 'common/FormItem'
import Link from 'common/Link'
interface Inputs {
    locationName: string
}

const Index: FC = () => {
    const [title] = useState<string>('Quản lý Địa điểm')
    const countDatatable = useRecoilValue(countDataTable)
    const {control, handleSubmit} = useForm<Inputs>({
        defaultValues: {
            locationName: '',
        },
    })
    const watchLocationNameFilter = useWatch({
        control,
        name: 'locationName',
    })
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
    })
    const columns: ColumnsType<LocationEntity> = [
        {
            dataIndex: 'locationName',
            title: 'Tên địa điểm',

        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align:'center',
            render: (value) => formatDate(value),

        },
        {
            dataIndex: 'updateAt',
            title: 'Cập nhật lần cuối',
            align:'center',
            render: (value) => formatDate(value),
        },
    ]
    useEffect(() => {
        if (!watchLocationNameFilter)
            setTableServiceParams((prevState) => ({
                ...prevState,
                filter: [],
            }))
    }, [watchLocationNameFilter])
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        const filter = []
        if (data?.locationName)
            filter.push(`locationName||$cont||${data?.locationName}`)

        setTableServiceParams((prevState) => ({
            ...prevState,
            filter: filter,
        }))
    }
    const onReset = () => {
        setTableServiceParams({
            sort: ['createdAt,DESC'],
        })
    }
    return (
        <Content
            title={title}
        >
            <Card>
                <Form
                    autoComplete={'off'}
                    onFinish={handleSubmit(onSubmit)}
                    onReset={onReset}
                >
                    <Row gutter={[8, 8]}>
                        <Col xs={24} sm={24} md={24} lg={12}>
                            <Form.Item label={'Tên địa điểm'}>
                                <Controller
                                    control={control}
                                    name={'locationName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            placeholder={'Nhập Tên địa điểm tìm kiếm'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                            <FormItem>
                                <Space>
                                    <Button htmlType={'reset'}>Đặt lại</Button>
                                    <Button type={'primary'} htmlType={'submit'}>
                                        Tìm kiếm
                                    </Button>
                                </Space>
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </Card>
            <Card
                title={<span className={'text-2xl'}>{countDatatable} Địa điểm</span>}
                extra={
                    <Link href={'/location/create'}>
                        <Button
                            type={'primary'}>
                            Thêm mới
                        </Button>
                    </Link>
                }
            >
                <DataTable
                    service={LocationService.getManyBase}
                    serviceParams={tableServiceParams}
                    deleteService={LocationService.deleteOneBase}
                    columns={columns}
                    action={['edit', 'delete']}
                    path={'/location'}
                />
            </Card>
        </Content>
    )
}

export default Index