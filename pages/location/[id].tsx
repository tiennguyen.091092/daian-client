import {FC, useEffect, useState} from 'react'
import {useRouter} from 'next/router'
import {loadingState} from 'recoil/Atoms'
import {useSetRecoilState} from 'recoil'
import {alertError, modifyEntity} from 'utils'
import {LocationEntity, LocationService} from 'services'
import {yupResolver} from '@hookform/resolvers/yup'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import FooterBar from 'components/layout/AdminLayout/FooterBar'
import Content from 'components/layout/AdminLayout/Content'
import {Button, Card, Col, Form, Input} from 'antd'
import FormItem from 'common/FormItem'
interface Inputs {
    location: LocationEntity
}
const schema = yup.object().shape({
    location: yup.object().shape({
        locationName: yup.string().required('Chưa nhập Tên địa điểm'),
    }),
})
const EditLocation: FC = () => {
    const [title, setTitle] = useState<string>('Cập nhật Địa điểm')
    const setIsLoading = useSetRecoilState(loadingState)
    const router = useRouter()
    const {
        control,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
    })
    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Thêm mới Địa điểm')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                LocationService.getOneBase({
                    id: Number(router?.query?.id),
                }),
            ])
                .then(([locationResponse]) => {
                    setValue('location', locationResponse)
                    setIsLoading(false)
                })
                .catch((error) => {
                    alertError(error)
                    setIsLoading(false)
                })
        }
    }, [router])

    const onSubmit: SubmitHandler<Inputs> = (data) => {
        modifyEntity(LocationService, data.location, title, () => {
            return router.push(`/location`)
        }).then()
    }

    return (
        <Content title={title}  onBack={() => router.push('/location')}>
            <Card>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    onFinish={handleSubmit(onSubmit)} autoComplete={'off'}
                >
                    <Form.Item
                        label={<span>Tên</span>}
                        validateStatus={errors.location?.locationName && 'error'}
                        required={true}
                        help={errors.location?.locationName && errors.location?.locationName?.message}
                    >
                        <Controller
                            control={control}
                            name={'location.locationName'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Tên địa điểm"
                                />
                            )}
                        />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Lưu lại
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Content>
    )
}

export default EditLocation