import Head from 'next/head'

const Index: React.FC = () => {
    return (
        <div>
            <Head>
                <title>DAIAN Admin</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>
        </div>
    )
}
export default Index