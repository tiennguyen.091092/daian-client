import React, {FC, useEffect, useState} from 'react'
import {countDataTable} from 'recoil/Atoms'
import {useRecoilValue} from 'recoil'
import {Controller, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {formatDate, getTextTypeCustomer} from 'utils'
import {ColumnsType} from 'antd/es/table'
import {CustomerEntity, CustomerService, EnumCustomerEntityCustomerGender} from 'services'
import {Button, Card, Col, Form, Input, Row, Space} from 'antd'
import DataTable from 'common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import FormItem from 'common/FormItem'
import Link from 'common/Link'

interface Inputs {
    customerName: string
}

const Index: FC = () => {
    const [title] = useState<string>('Quản lý Khách hàng')
    const countDatatable = useRecoilValue(countDataTable)
    const {control, handleSubmit} = useForm<Inputs>({
        defaultValues: {
            customerName: '',
        },
    })
    const watchCustomerNameFilter = useWatch({
        control,
        name: 'customerName',
    })
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
    })
    const columns: ColumnsType<CustomerEntity> = [
        {
            dataIndex: 'customerType',
            title: 'Loại khách hàng',
            render: (value, record) => getTextTypeCustomer(value),
        },
        {
            dataIndex: 'customerName',
            title: 'Tên khách hàng',
            // render:(value,record)=>{
            //     let strCusName = ''
            //     switch (record.customerGender){
            //         case EnumCustomerEntityCustomerGender.other :
            //             strCusName = value
            //             break
            //         case EnumCustomerEntityCustomerGender.female :
            //             strCusName = `Chị ${value}`
            //             break
            //         case EnumCustomerEntityCustomerGender.male :
            //             strCusName = `Anh ${value}`
            //             break
            //
            //     }
            //     return strCusName
            // }
        },
        {
            dataIndex: 'customerPhone',
            title: 'Điện thoại',
        },
        {
            dataIndex: 'customerAddress',
            title: 'Địa chỉ',
        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align:'center',
            render: (value) => formatDate(value),
        },
        {
            dataIndex: 'updateAt',
            title: 'Cập nhật lần cuối',
            align:'center',
            render: (value) => formatDate(value),
        },
    ]
    useEffect(() => {
        if (!watchCustomerNameFilter)
            setTableServiceParams((prevState) => ({
                ...prevState,
                filter: [],
            }))
    }, [watchCustomerNameFilter])
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        const filter = []
        const or = []
        if (data?.customerName){
            filter.push(`customerName||$cont||${data?.customerName}`)
            or.push(`customerPhone||$cont||${data?.customerName}`)
        }
        setTableServiceParams((prevState) => ({
            ...prevState,
            filter: filter,
            or:or
        }))
    }
    const onReset = () => {
        setTableServiceParams({
            sort: ['createdAt,DESC'],
        })
    }
    return (
        <Content
            title={title}
        >
            <Card>
                <Form
                    autoComplete={'off'}
                    onFinish={handleSubmit(onSubmit)}
                    onReset={onReset}
                >
                    <Row gutter={[8, 8]}>
                        <Col xs={24} sm={24} md={24} lg={12}>
                            <Form.Item label={'Khách hàng'}>
                                <Controller
                                    control={control}
                                    name={'customerName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            placeholder={'Nhập Tên khách hàng tìm kiếm'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                            <FormItem>
                                <Space>
                                    <Button htmlType={'reset'}>Đặt lại</Button>
                                    <Button type={'primary'} htmlType={'submit'}>
                                        Tìm kiếm
                                    </Button>
                                </Space>
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </Card>
            <Card
                title={<span className={'text-2xl'}>{countDatatable} khách hàng</span>}
                extra={
                    <Link href={'/customer/create'}>
                        <Button
                            type={'primary'}>
                            Thêm mới
                        </Button>
                    </Link>
                }
            >
                <DataTable
                    service={CustomerService.getManyBase}
                    serviceParams={tableServiceParams}
                    deleteService={CustomerService.deleteOneBase}
                    columns={columns}
                    action={['edit', 'delete']}
                    path={'/customer'}
                />
            </Card>
        </Content>
    )
}

export default Index