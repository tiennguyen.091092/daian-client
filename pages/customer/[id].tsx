import {FC, useEffect, useState} from 'react'
import {useRouter} from 'next/router'
import {loadingState} from 'recoil/Atoms'
import {useSetRecoilState} from 'recoil'
import {alertError, modifyEntity} from 'utils'
import {
    CustomerEntity,
    CustomerService,
    EnumCustomerEntityCustomerGender,
    EnumCustomerEntityCustomerType,
    LocationService
} from 'services'
import {yupResolver} from '@hookform/resolvers/yup'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import Content from 'components/layout/AdminLayout/Content'
import {Button, Card, Form, Input,Select} from 'antd'

interface Inputs {
    customer: CustomerEntity
}
const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema = yup.object().shape({
    customer: yup.object().shape({
        customerName: yup.string().required('Chưa nhập Tên khách hàng'),
        customerPhone: yup
            .string()
            .required('Chưa nhập số điện thoại')
            .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),

    }),
})

const EditCustomer: FC = () => {
    const [title, setTitle] = useState<string>('Cập nhật Khách hàng')
    const setIsLoading = useSetRecoilState(loadingState)
    const router = useRouter()
    const {
        control,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
    })
    const { TextArea } = Input
    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Thêm mới Khách hàng')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                CustomerService.getOneBase({
                    id: Number(router?.query?.id),
                }),
            ])
                .then(([responseData]) => {
                    setValue('customer', responseData)
                    setIsLoading(false)
                })
                .catch((error) => {
                    alertError(error)
                    setIsLoading(false)
                })
        }
    }, [router])

    const onSubmit: SubmitHandler<Inputs> = (data) => {
        modifyEntity(CustomerService, data.customer, title, () => {
            return router.push(`/customer`)
        }).then()
    }

    return (
        <Content title={title}  onBack={() => router.push('/customer')}>
            <Card>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    onFinish={handleSubmit(onSubmit)} autoComplete={'off'}
                >
                    <Form.Item label={'Loại khách hàng'}>
                        <Controller
                            name={'customer.customerType'}
                            control={control}
                            defaultValue={EnumCustomerEntityCustomerType.personal}
                            render={({ field }) => (
                                <Select
                                    showSearch
                                    placeholder="Vui lòng chọn Loại khách hàng"
                                    size={'middle'}
                                    {...field}
                                >
                                    <Select.Option value={EnumCustomerEntityCustomerType.personal}>Cá nhân
                                    </Select.Option>
                                    <Select.Option value={EnumCustomerEntityCustomerType.enterprise}>
                                        Doanh nghiệp
                                    </Select.Option>
                                </Select>
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Tên khách hàng</span>}
                        validateStatus={errors.customer?.customerName && 'error'}
                        required={true}
                        help={errors.customer?.customerName && errors.customer?.customerName?.message}
                    >
                        <Controller
                            control={control}
                            name={'customer.customerName'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Tên khách hàng"
                                />
                            )}
                        />

                    </Form.Item>
                    <Form.Item
                        label={<span>Số điện thoại </span>}
                        required={true}
                    >
                        <Controller
                            control={control}
                            name={'customer.customerPhone'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Số điện thoại"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item label={'Giới tính'}>
                        <Controller
                            name={'customer.customerGender'}
                            control={control}
                            render={({ field }) => (
                                <Select
                                    showSearch
                                    placeholder="Vui lòng chọn giới tính"
                                    size={'middle'}
                                    {...field}
                                >
                                    <Select.Option value={EnumCustomerEntityCustomerGender.other}>
                                        Khác
                                    </Select.Option>
                                    <Select.Option value={EnumCustomerEntityCustomerGender.male}>
                                        Nam
                                    </Select.Option>
                                    <Select.Option value={EnumCustomerEntityCustomerGender.female}>
                                        Nữ
                                    </Select.Option>
                                </Select>
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Địa chỉ</span>}
                    >
                        <Controller
                            control={control}
                            name={'customer.customerAddress'}
                            render={({ field }) => (
                                <TextArea
                                    {...field}
                                    placeholder="Địa chỉ khách hàng"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Lưu lại
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Content>
    )
}

export default EditCustomer