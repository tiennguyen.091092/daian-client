import React, { FC, useEffect } from 'react'
import Head from 'next/head'
import { setCookie } from 'nookies'
import { alertError } from 'utils'
import axios, { AxiosRequestConfig } from 'axios'
import { serviceOptions } from 'services/serviceOptions'
import { Button, Col, Form, Input, Row } from 'antd'
import { LockOutlined, PhoneOutlined } from '@ant-design/icons'
import styles from './auth.module.css'
import * as yup from 'yup'
import { SchemaOf } from 'yup'
import { Controller, SubmitHandler, useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import { useRouter } from 'next/router'
import { authState } from 'recoil/Atoms'
import { useSetRecoilState } from 'recoil'
import jwt_decode from 'jwt-decode'
import {AuthService, LoginDto} from "services";

interface Inputs {
  user: LoginDto
}

const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema: SchemaOf<Inputs> = yup.object().shape({
  user: yup.object().shape({
    tel: yup
      .string()
      .required('Chưa nhập số điện thoại')
      .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
    password: yup
      .string()
      .required('Chưa nhập mật khẩu.')
      .min(8, 'Mật khẩu quá ngắn - tối thiểu phải có 8 ký tự.'),
  }),
})

const Login: FC = () => {
  const form = useForm<Inputs>({ mode: 'all', resolver: yupResolver(schema) })
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = form
  const router = useRouter()
  const setAuthState = useSetRecoilState(authState)
  useEffect(() => {
    const axiosConfig: AxiosRequestConfig = {
      baseURL: process.env.NEXT_PUBLIC_API_URL || 'http://localhost:4000/',
      timeout: 60000, // 1 phút
    }
    serviceOptions.axios = axios.create(axiosConfig)
  }, [])

  const onSubmit: SubmitHandler<Inputs> = (data) => {
    AuthService.authControllerLogin({
      body: data.user,
    })
      .then((response) => {
        setCookie(null, 'token', String(response), {
          maxAge: 30 * 24 * 60 * 60,
          path: '/',
        })
        setAuthState(jwt_decode(response))
        window.location.href = '/'
      })
      .catch((error) => alertError(error))
  }
  return (
    <>
      <Head>
        <title>Đăng nhập - Đại An </title>
      </Head>
      <div className={styles.loginContainer}>
        <div
          className={`flex items-center justify-center py-32 px-4 sm:px-6 lg:px-8`}
        >
          <div className="max-w-sm w-full space-y-8">
            <Form className="mt-8 space-y-6" onFinish={handleSubmit(onSubmit)}>
              <div className="py-6 text-center w-full">
                <img
                  className={styles.logo}
                   src={"/images/logo.png"}
                  width={'50%'}
                />
              </div>
              <div className="rounded-md shadow-sm-space-y-px space-y-4">
                <Form.Item
                  validateStatus={errors.user?.tel && 'error'}
                  help={errors.user?.tel && errors.user?.tel?.message}
                >
                  <Controller
                    control={control}
                    name={'user.tel'}
                    render={({ field }) => (
                      <Input
                        {...field}
                        size="large"
                        placeholder="Số điện thoại"
                        prefix={<PhoneOutlined />}
                      />
                    )}
                  />
                </Form.Item>
                <Form.Item
                  validateStatus={errors.user?.password && 'error'}
                  help={errors.user?.password && errors.user?.password?.message}
                >
                  <Controller
                    control={control}
                    name={'user.password'}
                    render={({ field }) => (
                      <Input.Password
                        {...field}
                        size="large"
                        placeholder="Mật khẩu"
                        prefix={<LockOutlined />}
                      />
                    )}
                  />
                </Form.Item>
                {/*<Row>*/}
                {/*  <Col span={24}>*/}
                {/*    <div className=" mt-1">*/}
                {/*      <a className="font-semibold" href="#">*/}
                {/*        &nbsp; Quên mật khẩu?*/}
                {/*      </a>*/}
                {/*    </div>*/}
                {/*  </Col>*/}
                {/*</Row>*/}

                {/*<div*/}
                {/*  className='linkAuth text-right'>*/}
                {/*  <a className='font-semibold' href='/forgot-password'>*/}
                {/*    &nbsp;  Forgot your password?*/}
                {/*  </a>*/}
                {/*</div>*/}
                <Form.Item>
                  <Button
                    type="primary"
                    size={'large'}
                    htmlType={'submit'}
                    className={'w-full'}
                  >
                    Đăng nhập
                  </Button>
                </Form.Item>
                {/*<Form.Item>*/}
                {/*  <Button*/}
                {/*    type="default"*/}
                {/*    size={'large'}*/}
                {/*    className={'w-full'}*/}
                {/*    onClick={() => {*/}
                {/*      window.location.href = '/auth/register'*/}
                {/*    }}*/}
                {/*  >*/}
                {/*    Đăng ký*/}
                {/*  </Button>*/}
                {/*</Form.Item>*/}
              </div>
            </Form>
          </div>
        </div>
      </div>
    </>
  )
}

export default Login
