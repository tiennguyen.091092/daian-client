import {FC, useEffect, useState} from 'react'
import {useRouter} from 'next/router'
import {loadingState} from 'recoil/Atoms'
import {useSetRecoilState} from 'recoil'
import {alertError, modifyEntity} from 'utils'
import {CustomerService, DriverCarEntity, DriverCarService, EnumDriverCarEntityCarType} from 'services'
import {yupResolver} from '@hookform/resolvers/yup'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import Content from 'components/layout/AdminLayout/Content'
import {Button, Card, Form, Input, Select} from 'antd'

interface Inputs {
    driverCar: DriverCarEntity
}
const schema = yup.object().shape({
    driverCar: yup.object().shape({
        driverName: yup.string().required('Chưa nhập Tên tài xế'),
    }),
})
const EditDriverCar: FC = () => {
    const [title, setTitle] = useState<string>('Cập nhật Xe - Tài xế')
    const setIsLoading = useSetRecoilState(loadingState)
    const router = useRouter()
    const {
        control,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
    })
    const { TextArea } = Input
    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Thêm mới Xe - tài xế')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                DriverCarService.getOneBase({
                    id: Number(router?.query?.id),
                }),
            ])
                .then(([responseData]) => {
                    setValue('driverCar', responseData)
                    setIsLoading(false)
                })
                .catch((error) => {
                    alertError(error)
                    setIsLoading(false)
                })
        }
    }, [router])

    const onSubmit: SubmitHandler<Inputs> = (data) => {
        modifyEntity(DriverCarService, data.driverCar, title, () => {
            return router.push(`/driver-car`)
        }).then()
    }

    return (
        <Content title={title}  onBack={() => router.push('/driver-car')}>
            <Card>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    onFinish={handleSubmit(onSubmit)} autoComplete={'off'}
                >
                    <Form.Item label={'Loại xe'}>
                        <Controller
                            name={'driverCar.carType'}
                            control={control}
                            render={({ field }) => (
                                <Select
                                    showSearch
                                    placeholder="Vui lòng chọn Loại xe"
                                    size={'middle'}
                                    {...field}
                                >
                                    <Select.Option value={EnumDriverCarEntityCarType.passenger}>Xe khách
                                    </Select.Option>
                                    <Select.Option value={EnumDriverCarEntityCarType.cargo}>Xe hàng
                                    </Select.Option>
                                    <Select.Option value={EnumDriverCarEntityCarType.other}>Khác
                                    </Select.Option>
                                </Select>
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Tên tài xế</span>}
                        validateStatus={errors.driverCar?.driverName && 'error'}
                        required={true}
                        help={errors.driverCar?.driverName && errors.driverCar?.driverName?.message}
                    >
                        <Controller
                            control={control}
                            name={'driverCar.driverName'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Tên tài xế"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Số điện thoại </span>}
                    >
                        <Controller
                            control={control}
                            name={'driverCar.driverPhone'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Số điện thoại"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Biển số xe </span>}
                    >
                        <Controller
                            control={control}
                            name={'driverCar.licensePlates'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Biển số xe"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Lưu lại
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Content>
    )
}

export default EditDriverCar