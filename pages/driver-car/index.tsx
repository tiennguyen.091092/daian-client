import React, {FC, useEffect, useState} from 'react'
import {countDataTable} from 'recoil/Atoms'
import {useRecoilValue} from 'recoil'
import {Controller, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {formatDate, getTextTypeCustomer, getTextTypeDriverCar} from 'utils'
import {ColumnsType} from 'antd/es/table'
import {DriverCarEntity, DriverCarService, LocationEntity, LocationService} from 'services'
import {Button, Card, Col, Form, Input, Row, Space} from 'antd'
import DataTable from 'common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import FormItem from 'common/FormItem'
import Link from 'common/Link'
interface Inputs {
    driverName: string
}

const Index: FC = () => {
    const [title] = useState<string>('Quản lý Xe - Tài xế')
    const countDatatable = useRecoilValue(countDataTable)
    const {control, handleSubmit} = useForm<Inputs>({
        defaultValues: {
            driverName: '',
        },
    })
    const watchDriverNameFilter = useWatch({
        control,
        name: 'driverName',
    })
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
    })
    const columns: ColumnsType<DriverCarEntity> = [
        {
            dataIndex: 'carType',
            title: 'Loại xe',
            render: (value, record) => getTextTypeDriverCar(value),
        },
        {
            dataIndex: 'driverName',
            title: 'Tên tài xế',

        },
        {
            dataIndex: 'licensePlates',
            title: 'Biển số xe',

        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align:'center',
            render: (value) => formatDate(value),

        },
        {
            dataIndex: 'updateAt',
            title: 'Cập nhật lần cuối',
            align:'center',
            render: (value) => formatDate(value),
        },
    ]
    useEffect(() => {
        if (!watchDriverNameFilter)
            setTableServiceParams((prevState) => ({
                ...prevState,
                filter: [],
            }))
    }, [watchDriverNameFilter])
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        const filter = []
        if (data?.driverName)
            filter.push(`driverName||$cont||${data?.driverName}`)

        setTableServiceParams((prevState) => ({
            ...prevState,
            filter: filter,
        }))
    }
    const onReset = () => {
        setTableServiceParams({
            sort: ['createdAt,DESC'],
        })
    }
    return (
        <Content
            title={title}
        >
            <Card>
                <Form
                    autoComplete={'off'}
                    onFinish={handleSubmit(onSubmit)}
                    onReset={onReset}
                >
                    <Row gutter={[8, 8]}>
                        <Col xs={24} sm={24} md={24} lg={12}>
                            <Form.Item label={'Tên tài xế'}>
                                <Controller
                                    control={control}
                                    name={'driverName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            placeholder={'Nhập Tên tài xế tìm kiếm'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                            <FormItem>
                                <Space>
                                    <Button htmlType={'reset'}>Đặt lại</Button>
                                    <Button type={'primary'} htmlType={'submit'}>
                                        Tìm kiếm
                                    </Button>
                                </Space>
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </Card>
            <Card
                title={<span className={'text-2xl'}>{countDatatable} Xe - Tài xế</span>}
                extra={
                    <Link href={'/driver-car/create'}>
                        <Button
                            type={'primary'}>
                            Thêm mới
                        </Button>
                    </Link>
                }
            >
                <DataTable
                    service={DriverCarService.getManyBase}
                    serviceParams={tableServiceParams}
                    deleteService={DriverCarService.deleteOneBase}
                    columns={columns}
                    action={['edit', 'delete']}
                    path={'/driver-car'}
                />
            </Card>
        </Content>
    )
}

export default Index