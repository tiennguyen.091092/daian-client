import {FC, useEffect, useRef, useState} from 'react'
import {useReactToPrint} from 'react-to-print'
import {OnOrderEntity, OnOrderService} from 'services'
import {useRouter} from 'next/router'
import OnOrderPrint from 'pages/on-order/print/index'

const Print:FC = ()=>{
    const [onOrder,setOnOrder] = useState<OnOrderEntity>()
    const router = useRouter()
    const handlePrint = useReactToPrint({
        // @ts-ignore
        content: () => invoicePrintRef.current,
    })
    useEffect(()=>{
        handlePrint()
    },[onOrder])
    useEffect(()=>{
        if (router.query?.id) {
            OnOrderService.getOneBase({
                id: Number(router?.query?.id),
                join: [
                    'route',
                    'driverCar',
                    'orders',
                    'orders.customerSend',
                    'orders.customerReceived',
                    'orders.location'
                ]
            }).then((response) =>
                setOnOrder(response)
            )
        }
        handlePrint()
    },[router.query?.id])
    const invoicePrintRef = useRef()
    return (<OnOrderPrint ref={invoicePrintRef} onOrder={onOrder} />)
}
export default Print
