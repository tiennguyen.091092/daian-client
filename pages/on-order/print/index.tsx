import * as React from 'react'
import {EnumOrderEntityPaymentType, OnOrderEntity, OrderEntity,} from 'services'
import {Col, Row, Typography} from 'antd'
import {formatCurrency, formatDate, formatDateDDMMYYYY, getTextTypeDriverCar} from 'utils'

interface IProps {
    onOrder: OnOrderEntity | undefined
}

type TState = {
}
const {Title, Text} = Typography

export default class OnOrderPrint extends React.Component<IProps, TState> {
    constructor(props) {
        super(props)
        this.state = {
        }
    }

    canvasEl

    componentDidMount() {

    }


    setRef = (ref) => (this.canvasEl = ref)

    render() {
        const {onOrder} = this.props
        const groupOrderByLocation = this.props.onOrder?.orders?.reduce((group, order) => {
            const {locationId} = order
            group[locationId] = group[locationId] ?? []
            group[locationId].push(order)
            return group
        }, {})
        const  totalCollectedMoney= this?.props?.onOrder?.orders?.reduce(function (accumulator, curValue) {
            return accumulator + curValue.collectedMoney
        }, 0)
       const  totalNotCollectedMoney= this?.props?.onOrder?.orders?.reduce(function (accumulator, curValue) {
            return accumulator + curValue.notCollectedMoney
        }, 0)
        const  totalCashCollection= this?.props?.onOrder?.orders?.reduce(function (accumulator, curValue) {
            return accumulator + curValue.cashCollection
        }, 0)
        const arrayCount = []
        return (
            <>
                <Row gutter={16} className={'ml-1 mr-1 p-5'}>
                    {/* Trang 2 */}
                    <Row className={'w-full'}>
                        <Col span={12}>
                            <Col span={24}><Title level={3} className={'m-0'}>Đại An Logistics</Title></Col>
                            <Col span={24}><Title level={4} className={'m-0'}>Hotline: 0932329955</Title></Col>
                            <Col span={24} className={'text-left'}>
                                <Text>111 Phố Thiên Hiền, Mỹ Đình</Text>
                            </Col>
                        </Col>
                        <Col span={12} className={'text-right'}>
                            <Col span={24} className={'text-right'}>
                                <b>Mỹ Đình - {formatDate(new Date())}</b>
                            </Col>
                            <Col span={24} className={'text-right'}>
                                <b>Tuyến: {onOrder?.route?.name}</b> <Text>Chuyến:
                                ({onOrder?.time} - {formatDateDDMMYYYY(onOrder?.date)})</Text>
                            </Col>
                            <Col span={24} className={'text-right'}>
                                <Text>Tài xế {onOrder?.driverCar.driverName} Số
                                    xe: {onOrder?.driverCar.licensePlates}</Text>
                            </Col>
                            <Col span={24} className={'text-right'}>
                                <Text>Loại xe: {getTextTypeDriverCar(onOrder?.driverCar.carType)}</Text>
                            </Col>
                        </Col>

                        {/* Table order*/}
                        <Col span={24} className={'w-full'}>
                            <table className={'w-full border border-black'}>
                                <tr className={'border border-black bg-slate-300'}>
                                    <th className={'border border-black'}>STT</th>
                                    <th className={'border border-black'}>Người gửi</th>
                                    <th className={'border border-black'}>Người nhận</th>
                                    <th className={'border border-black'}>Tên hàng</th>
                                    <th className={'border border-black'}>SL</th>
                                    <th className={'border border-black'}>Đã thu</th>
                                    <th className={'border border-black'}>Chưa thu</th>
                                    <th className={'border border-black'}>Thu hộ</th>
                                    <th className={'border border-black'}>ĐC Giao</th>
                                    <th className={'border border-black'}>Mã GD</th>
                                    <th className={'border border-black'}>Ký nhận</th>
                                </tr>
                                {groupOrderByLocation && Object.keys(groupOrderByLocation)?.map((locationId, index) => {
                                        const orderByLocations = onOrder?.orders?.filter((item) => item.locationId === Number(locationId))
                                        const countOrderByLocation = orderByLocations.length

                                        const totalCollectionMoney = orderByLocations.reduce(function (accumulator, curValue) {
                                            return accumulator + curValue.collectedMoney
                                        }, 0)
                                        const totalNotCollectionMoney = orderByLocations.reduce(function (accumulator, curValue) {
                                            return accumulator + curValue.notCollectedMoney
                                        }, 0)
                                        const totalQuantity = orderByLocations.reduce(function (accumulator, curValue) {
                                            return accumulator + curValue.quantity
                                        }, 0)
                                        const totalCashCollection = orderByLocations.reduce(function (accumulator, curValue) {
                                            return accumulator + curValue.cashCollection
                                        }, 0)
                                        // this.setState({
                                        //     ...this.state,
                                        //     totalCollectedMoney:totalCollectionMoney,
                                        //     totalNotCollectedMoney:totalNotCollectionMoney
                                        //})
                                        return (
                                            <>
                                                <tr className={'border border-black'}>
                                                    <td colSpan={10}>
                                                        <b>{
                                                            onOrder?.orders?.find((item) => item.locationId === Number(locationId)).location.locationName
                                                        }</b>
                                                        <span className={'ml-8'}> Đơn: {countOrderByLocation} </span>
                                                        | <span> SL: {countOrderByLocation} </span>
                                                        | <span> CR: {formatCurrency(totalCollectionMoney)} </span>
                                                        | <span> CC: {formatCurrency(totalNotCollectionMoney)} </span>
                                                        | <span> Tổng cước: {formatCurrency(totalCollectionMoney + totalNotCollectionMoney)} </span>
                                                    </td>
                                                </tr>
                                                {orderByLocations?.map((orderByLocation, orByLocationIndex) => {
                                                    arrayCount.push(orByLocationIndex)
                                                    return (
                                                        <tr className={'border border-black'}
                                                            key={`orderItem-${orderByLocation.id}`}>
                                                            <td className={'border border-black text-center'}>{arrayCount.length}</td>
                                                            <td className={'border border-black p-1'}>
                                                                <span> {orderByLocation?.customerSend?.customerName}</span>
                                                                <br/>
                                                                <span> {orderByLocation?.customerSend?.customerPhone}</span>
                                                            </td>
                                                            <td className={'border border-black p-1'}>
                                                                <span> {orderByLocation?.customerReceived?.customerName}</span>
                                                                <br/>
                                                                <span> {orderByLocation?.customerReceived?.customerPhone}</span>
                                                            </td>
                                                            <td className={'border border-black p-1'}>{orderByLocation?.itemName}</td>
                                                            <td className={'border border-black text-center'}>{orderByLocation?.quantity}</td>
                                                            <td className={'border border-black text-center'}>{orderByLocation?.collectedMoney ? formatCurrency(orderByLocation?.collectedMoney) : ''}</td>
                                                            <td className={'border border-black text-center'}>{orderByLocation?.notCollectedMoney ? formatCurrency(orderByLocation?.notCollectedMoney) : ''}</td>
                                                            <td className={'border border-black text-center'}>{orderByLocation?.cashCollection ? formatCurrency(orderByLocation?.cashCollection) : ''}</td>
                                                            <td className={'border border-black text-center'}>{orderByLocation?.address}</td>
                                                            <td className={'border border-black text-center'}>{orderByLocation?.orderCode}</td>
                                                        </tr>

                                                    )
                                                })
                                                }
                                                <tr className={'border border-black'}>
                                                    <td className={'border border-black'}><b>Tổng</b></td>
                                                    <td className={'border border-black'}></td>
                                                    <td className={'border border-black'}></td>
                                                    <td className={'border border-black'}></td>
                                                    <td className={'border border-black text-center'}><b>{totalQuantity}</b>
                                                    </td>
                                                    <td className={'border border-black text-center'}>
                                                        <b>{formatCurrency(totalCollectionMoney)}</b></td>
                                                    <td className={'border border-black text-center'}>
                                                        <b>{formatCurrency(totalNotCollectionMoney)}</b></td>
                                                    <td className={'border border-black text-center'}>
                                                        <b>{formatCurrency(totalCashCollection)}</b></td>
                                                    <td className={'border border-black'}></td>
                                                    <td className={'border border-black'}></td>
                                                </tr>
                                            </>
                                        )
                                    }
                                )}
                            </table>
                        </Col>
                        <Row className={'mt-6 w-full'}>
                            <Col span={8} className={'text-center'}>
                                <Text>Đã thu:</Text> <b>{formatCurrency(totalCollectedMoney)}</b> / <Text>Chưa thu:</Text> <b>{formatCurrency(totalNotCollectedMoney)}</b><br/>
                                <Text>VP Gửi</Text><br/>
                                <Text>(Ký & ghi rõ họ tên)</Text>
                            </Col>
                            <Col span={8} className={'text-center'}>

                                <Text>Thu hộ:</Text> <b>{formatCurrency(totalCashCollection)}</b><br/>
                                <Text>Tài xế</Text><br/>
                                <Text>(Ký & ghi rõ họ tên)</Text>
                            </Col>
                            <Col span={8} className={'text-center'}>

                                <Text>Tổng Phải thu:</Text> <b>{formatCurrency(totalNotCollectedMoney+totalCashCollection)}</b><br/>
                                <Text>VP nhận</Text><br/>
                                <Text>(Ký & ghi rõ họ tên)</Text>
                            </Col>
                        </Row>
                    </Row>
                </Row>
            </>
        )
    }
}

