import React, {FC, useEffect, useState} from "react";
import EditOrder from "../order/[id]";
import {
    DriverCarEntity,
    DriverCarService, EnumChangeStatusDtoStatus, EnumOrderEntityStatus,
    OnOrderEntity,
    OnOrderService,
    OrderEntity, OrderService,
    RouteEntity,
    RouteService
} from "../../services";
import {Controller, SubmitHandler, useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup";
import * as yup from "yup";
import {useRouter} from "next/router";
import {Button, Card, Col, Form, Input, Popconfirm, Row, Select, Space, Table, Tag, TimePicker} from "antd";
import {ColumnsType} from "antd/es/table";
import {
    alertError,
    alertSuccess,
    filterOption,
    formatCurrency,
    formatDate,
    formatDateDDMMYYYY,
    getStatusOrder, modifyEntity
} from "../../utils";
import {useRecoilValue, useSetRecoilState} from "recoil";
import {authState, loadingState} from "../../recoil/Atoms";
import Content from "../../components/layout/AdminLayout/Content";
import DatePicker from "../../common/DatePicker";
import dayjs, {Dayjs} from "dayjs";
import DataTable from "../../common/DataTable";
import {ServiceParams} from "../../utils/interfaces";
import {DeleteOutlined} from "@ant-design/icons";

interface OnOrderInputs {
    onOrder: OnOrderEntity
    time?: Dayjs
}

const schema = yup.object().shape({
    onOrder: yup.object().shape({
        routeId: yup.string().required('Chưa chọn tuyến giao'),
        driverCarId: yup.string().required('Chưa chọn Xe - tài xế'),
    })
})
const EditOnOrder: FC = () => {
    const setIsLoading = useSetRecoilState(loadingState)
    const [title, setTitle] = useState<string>('Cập nhật chuyến hàng')
    const user = useRecoilValue(authState)
    const [routes, setRoutes] = useState<RouteEntity[]>()
    const [driverCars, setDriverCars] = useState<DriverCarEntity[]>()
    const [selectedOrders, setSelectedOrders] = useState<OrderEntity[]>()
    const [selectedNewOrders, setSelectedNewOrders] = useState<OrderEntity[]>()
    const router = useRouter()
    const [timerString, setTimerString] = useState('')
    const {
        control,
        handleSubmit,
        setValue,
        formState: {errors},
    } = useForm<OnOrderInputs>({
        mode: 'all',
        resolver: yupResolver(schema),
    })
    const {TextArea} = Input
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
        filter: [`status||$eq||${EnumOrderEntityStatus.created}`],
        join: [
            'createdBy',
            'customerSend',
            'branchOfficeSend',
            'customerReceived',
            'branchOfficeReceive',
            'location',
            'onOrder',
            'onOrder.route'
        ],
    })
    const columns: ColumnsType<OrderEntity> = [
        {
            dataIndex: 'orderCode',
            title: 'Mã đơn hàng',
        },
        {
            dataIndex: 'location',
            title: 'Điểm giao',
            render: (value, record) => record?.location?.locationName,
        },
        {
            dataIndex: 'customerSend',
            title: 'Người gửi',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerSend?.customerName}</b>
                        <br/>
                        <span>{record?.customerSend?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'customerReceive',
            title: 'Người nhận',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerReceived?.customerName}</b>
                        <br/>
                        <span>{record?.customerReceived?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'branchOfficeSend',
            title: 'VP Gửi',
            render: (value, record) => {
                return (<>
                        <b>{record?.branchOfficeSend?.branchOfficeName}</b>
                        <br/>
                        <span>{record?.branchOfficeSend?.branchOfficePhone}</span>
                        <br/>
                        <span>{record?.branchOfficeSend?.branchOfficeAddress}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'branchOfficeReceive',
            title: 'VP Nhận',
            render: (value, record) => {
                return (<>
                        <b>{record?.branchOfficeReceive?.branchOfficeName}</b>
                        <br/>
                        <span>{record?.branchOfficeReceive?.branchOfficePhone}</span>
                        <br/>
                        <span>{record?.branchOfficeReceive?.branchOfficeAddress}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'itemName',
            title: 'Tên hàng hóa',
        },
        {
            dataIndex: 'quantity',
            title: 'Số lượng',
            align: 'center',
        },
        {
            dataIndex: 'orderMoney',
            title: 'Cước',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'collectedMoney',
            title: 'Đã thu',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'notCollectedMoney',
            title: 'Chưa thu',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align: 'center',
            render: (value) => formatDate(value),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            render: (value) =>
                value === EnumOrderEntityStatus.created ? (
                    <Tag color={'success'}>{getStatusOrder(value)}</Tag>
                ) : value === EnumOrderEntityStatus.on_order ? (
                    <Tag color="#108ee9">{getStatusOrder(value)}</Tag>
                ) : value === EnumOrderEntityStatus.off_order ? (
                    <Tag color={'error'}>{getStatusOrder(value)}</Tag>
                ) : null,
        },
        {
            title: 'Xuống hàng',
            dataIndex: 'id',
            render: (value, record) => {
                return (
                    <Space>
                        <Popconfirm
                            title="Bạn chắc chắn muốn xuống đơn hàng này?"
                            onConfirm={() => handleDelete(value, record.orderCode)}
                        >
                            <Button
                                shape={'circle'}
                                key={value}
                                icon={<DeleteOutlined/>}
                            />
                        </Popconfirm>
                    </Space>
                )
            }
        }
    ]
    const newOrderColumns: ColumnsType<OrderEntity> = [
        {
            dataIndex: 'onOrder',
            title: 'Chuyến',
            render: (value, record) => {
                return (<>
                        <b>{record?.onOrder?.route?.name}</b>
                        <br/>
                        <span>{record?.onOrder?.time}</span> <span>{formatDateDDMMYYYY(record?.onOrder?.date)}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'location',
            title: 'Điểm giao',
            render: (value, record) => record?.location?.locationName,
        },
        {
            dataIndex: 'customerSend',
            title: 'Người gửi',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerSend?.customerName}</b>
                        <br/>
                        <span>{record?.customerSend?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'customerReceive',
            title: 'Người nhận',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerReceived?.customerName}</b>
                        <br/>
                        <span>{record?.customerReceived?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'itemName',
            title: 'Tên hàng hóa',
        },
        {
            dataIndex: 'quantity',
            title: 'Số lượng',
            align: 'center',
        },
        {
            dataIndex: 'orderMoney',
            title: 'Cước',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'collectedMoney',
            title: 'Đã thu',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'notCollectedMoney',
            title: 'Chưa thu',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align: 'center',
            render: (value) => formatDate(value),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            render: (value) =>
                value === EnumOrderEntityStatus.created ? (
                    <Tag color={'success'}>{getStatusOrder(value)}</Tag>
                ) : value === EnumOrderEntityStatus.on_order ? (
                    <Tag color="#108ee9">{getStatusOrder(value)}</Tag>
                ) : value === EnumOrderEntityStatus.off_order ? (
                    <Tag color={'error'}>{getStatusOrder(value)}</Tag>
                ) : null,
        },

    ]
    useEffect(() => {
        Promise.all([
            RouteService.getManyBase({
                limit: 1000,
            }),
            DriverCarService.getManyBase({
                limit: 1000,
            }),
        ]).then(([routeResponse, driverCarResponse]) => {
                setRoutes(routeResponse.data)
                setDriverCars(driverCarResponse.data)
            }
        )
    }, [router])

    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Tạo mới chuyến hàng')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                OnOrderService.getOneBase({
                    id: Number(router?.query?.id),
                    join: ['orders', 'orders.createdBy',
                        'orders.customerSend',
                        'orders.customerReceived',
                        'orders.branchOfficeSend',
                        'orders.branchOfficeReceive',
                        'orders.location',
                    ]
                }),
                RouteService.getManyBase({
                    limit: 1000,
                }),
                DriverCarService.getManyBase({
                    limit: 1000,
                }),
            ]).then(([responseData, routeResponse, driverCarResponse]) => {
                    setValue('time', dayjs(responseData.time, 'HH:mm'))
                    setTimerString(responseData.time)
                    setValue('onOrder', {
                        id: responseData.id,
                        driverCarId: responseData.driverCarId,
                        date: new Date(responseData.date),
                        routeId: responseData.routeId,
                        note: responseData.note
                    })
                    setRoutes(routeResponse?.data)
                    setDriverCars(driverCarResponse?.data)
                    if (router.query?.id == 'create') {
                        setSelectedOrders([])
                    } else {
                        setSelectedOrders(responseData?.orders)
                    }

                    setIsLoading(false)
                }
            ).catch((error) => {
                alertError(error)
                setIsLoading(false)
            })
        }
    }, [router])
    const onSubmit: SubmitHandler<OnOrderInputs> = (data) => {
        const convertData = {
            ...data.onOrder,
            time: timerString,
            modifiedById: user.id
        }
        modifyEntity(OnOrderService, convertData, 'Cập nhật chuyến hàng', (response) => {
            selectedNewOrders?.map((orderItem) => {
                OrderService.orderControllerChangeStatus({
                    id: orderItem.id,
                    body: {
                        status: EnumChangeStatusDtoStatus.on_order,
                        onOrderId: response.id
                    }
                })
            })
            return router.push(`/on-order`)
        }).then()
    }
    const onChange = (time: Dayjs, timeString: string) => {
        setTimerString(timeString)
        setValue('time', time)
    }
    const handleDelete = (id: number, orderCode: string) => {
        OrderService.orderControllerOrderRemoveOnOrder({
            id: id,
        })
            .then(() => {
                alertSuccess(`Đã xuống đơn hàng ${orderCode} thành công`)
                setTableServiceParams({
                    sort: ['createdAt,DESC'],
                    filter: [`status||$eq||${EnumOrderEntityStatus.created}`],
                    join: [
                        'createdBy',
                        'customerSend',
                        'branchOfficeSend',
                        'customerReceived',
                        'branchOfficeReceive',
                        'location',
                        'onOrder',
                        'onOrder.route'
                    ],
                })
                const fillterSelectedsOrder = selectedOrders.filter(item => item.id !== id);
                setSelectedOrders(fillterSelectedsOrder)
            })
            .catch((error) => {
                return alertError(error)
            })
    }
    return (
        <Content title={title} onBack={() => router.push('/on-order')}>
            <Card>
                <Form
                    name="form_create_shipping_slip"
                    autoComplete={'off'}
                    className={'mt-4'}
                    labelCol={{span: 8}}
                    wrapperCol={{span: 16}}
                >
                    <Row className={'mt-4'}>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label={'Tuyến'}
                                       required={true}
                                       validateStatus={errors.onOrder?.routeId && 'error'}
                                       help={errors.onOrder?.routeId && errors?.onOrder?.routeId?.message}
                            >
                                <Controller
                                    name={'onOrder.routeId'}
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            {...field}
                                            placeholder={'Chọn tuyến'}
                                        >
                                            {routes?.map((route) => (
                                                <Select.Option value={route.id}
                                                               key={`route-${route.id}`}>
                                                    {route.name}
                                                </Select.Option>
                                            ))}
                                        </Select>
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label={'Ngày'}
                                       required={true}>
                                <Controller
                                    name={'onOrder.date'}
                                    control={control}
                                    render={({field}) => (
                                        // @ts-ignore
                                        <DatePicker
                                            {...field}
                                            allowClear
                                            format={'dd/MM/y'}
                                            placeholder={'Ngày'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label={'Chuyến'} required={true}>
                                <Controller
                                    name={'time'}
                                    control={control}
                                    render={({field}) => (
                                        // @ts-ignore
                                        <TimePicker
                                            {...field}
                                            allowClear
                                            format={'HH:mm'}
                                            placeholder={'Chuyến'}
                                            className={'w-full'}
                                            onChange={onChange}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item
                                required={true}
                                label={<span>Chọn xe - tài xế </span>}
                                validateStatus={errors.onOrder?.driverCarId && 'error'}
                                help={errors.onOrder?.driverCarId && errors.onOrder?.driverCarId?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'onOrder.driverCarId'}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            {...field}
                                            placeholder={'Chọn xe - tài xế'}
                                        >
                                            {driverCars?.map((driveCar) => (
                                                <Select.Option value={driveCar.id}
                                                               key={`driverCar-${driveCar.id}`}>
                                                    {driveCar.driverName} - {driveCar.licensePlates}
                                                </Select.Option>
                                            ))}
                                        </Select>
                                    )}
                                />
                            </Form.Item> </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label="Ghi chú">
                                <Controller
                                    control={control}
                                    name={'onOrder.note'}
                                    render={({field}) => (
                                        <TextArea
                                            {...field}
                                            placeholder="Nhập ghi chú"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>

                    </Row>
                    <div className={'uppercase text-primary text-lg font-medium border-b mt-8'}>
                        Đơn hàng đã lên xe
                    </div>
                    <Table
                        dataSource={selectedOrders}
                        scroll={{x: 1000}}
                        size={'small'}
                        columns={columns}
                    />

                    <div className={'uppercase text-primary text-lg font-medium border-b mt-2'}>
                        Đơn hàng mới
                    </div>

                    <DataTable
                        service={OrderService.getManyBase}
                        serviceParams={tableServiceParams}
                        deleteService={OrderService.deleteOneBase}
                        columns={newOrderColumns}
                        rowSelection={{
                            type: 'checkbox',
                            onChange: (
                                selectedRowKeys: React.Key[],
                                selectedRows: OrderEntity[]
                            ) => setSelectedNewOrders(selectedRows),
                            selectedRowKeys: selectedNewOrders?.map(({id}) => id),
                            //@ts-ignore
                            getCheckboxProps: (record: OrderEntity) => ({
                                disabled: record.status === EnumOrderEntityStatus.on_order, // Column configuration not to be checked
                                id: record.id,
                            }),
                        }}
                    />

                    <Row className={'float-right'}>
                        <Button type="primary" size={'large'} htmlType="submit" onClick={handleSubmit(onSubmit)}>
                            Lưu cập nhật chuyến hàng
                        </Button>
                    </Row>
                </Form>
            </Card>
        </Content>
    )

}

export default EditOnOrder