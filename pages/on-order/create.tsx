import React, {FC, useEffect, useState} from 'react'
import {Button, Col, Form, Input, Modal, Row, Select, Table, TimePicker} from 'antd'
import {
    DriverCarEntity,
    DriverCarService,
    EnumChangeStatusDtoStatus,
    OnOrderEntity,
    OnOrderService,
    OrderEntity,
    OrderService,
    RouteEntity,
    RouteService
} from 'services'
import {ColumnsType} from 'antd/es/table'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import {useRouter} from 'next/router'
import {yupResolver} from '@hookform/resolvers/yup'
import DatePicker from 'common/DatePicker'
import {filterOption, formatCurrency, formatDate, modifyEntity} from 'utils'
import {Dayjs} from 'dayjs'
import {useRecoilValue} from "recoil";
import {authState} from "../../recoil/Atoms";

interface CreateOnOrderProps {
    selectedOrders?: OrderEntity[]
    isShowModal?: boolean
    setIsShowModal?: (value: boolean) => void
}

interface OnOrderInputs {
    onOrder: OnOrderEntity

}

const schema = yup.object().shape({
    onOrder: yup.object().shape({
        routeId: yup.string().required('Chưa chọn tuyến giao'),
        driverCarId: yup.string().required('Chưa chọn Xe - tài xế'),
    })
})
const CreateOnOrder: FC<CreateOnOrderProps> = (props) => {
    const router = useRouter()
    const user = useRecoilValue(authState)
    const {selectedOrders, isShowModal, setIsShowModal} = props
    const [routes, setRoutes] = useState<RouteEntity[]>()
    const [driverCars, setDriverCars] = useState<DriverCarEntity[]>()
    const [time, setTimer] = useState('')
    const {
        control,
        handleSubmit,
        formState: {errors},
    } = useForm<OnOrderInputs>({
        mode: 'all',
        resolver: yupResolver(schema),
        defaultValues:{
            onOrder:{
                date: new Date()
            }
        }
    })
    const {TextArea} = Input

    const columns: ColumnsType<OrderEntity> = [
        {
            dataIndex: 'orderCode',
            title: 'Mã đơn hàng',
        },
        {
            dataIndex: 'location',
            title: 'Điểm giao',
            render: (value, record) => record?.location?.locationName,
        },
        {
            dataIndex: 'customerSend',
            title: 'Người gửi',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerSend?.customerName}</b>
                        <br/>
                        <span>{record?.customerSend?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'customerReceive',
            title: 'Người nhận',
            render: (value, record) => {
                return (<>
                        <b>{record?.customerReceived?.customerName}</b>
                        <br/>
                        <span>{record?.customerReceived?.customerPhone}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'branchOfficeSend',
            title: 'VP Gửi',
            render: (value, record) => {
                return (<>
                        <b>{record?.branchOfficeSend?.branchOfficeName}</b>
                        <br/>
                        <span>{record?.branchOfficeSend?.branchOfficePhone}</span>
                        <br/>
                        <span>{record?.branchOfficeSend?.branchOfficeAddress}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'branchOfficeReceive',
            title: 'VP Nhận',
            render: (value, record) => {
                return (<>
                        <b>{record?.branchOfficeReceive?.branchOfficeName}</b>
                        <br/>
                        <span>{record?.branchOfficeReceive?.branchOfficePhone}</span>
                        <br/>
                        <span>{record?.branchOfficeReceive?.branchOfficeAddress}</span>
                    </>
                )
            }
        },
        {
            dataIndex: 'itemName',
            title: 'Tên hàng hóa',
        },
        {
            dataIndex: 'quantity',
            title: 'Số lượng',
            align: 'center',
        },
        {
            dataIndex: 'orderMoney',
            title: 'Cước',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'collectedMoney',
            title: 'Đã thu',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'notCollectedMoney',
            title: 'Chưa thu',
            align: 'right',
            render: (value, record) => formatCurrency(value),
        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align: 'center',
            render: (value) => formatDate(value),
        },
    ]
    const onSubmit: SubmitHandler<OnOrderInputs> = (data) => {
        const convertData = {
            ...data.onOrder,
            time: time,
            createdById: user.id,
            modifiedById:user.id
        }
        modifyEntity(OnOrderService, convertData, 'Lên hàng thành công', (response) => {
            selectedOrders?.map((orderItem) => {
                OrderService.orderControllerChangeStatus({
                    id: orderItem.id,
                    body: {
                        status : EnumChangeStatusDtoStatus.on_order,
                        onOrderId:response.id
                    }
                })
            })
            return router.push(`/on-order`)
        }).then()
    }

    useEffect(() => {
        Promise.all([
            RouteService.getManyBase({
                limit: 1000,
            }),
            DriverCarService.getManyBase({
                limit: 1000,
            }),
        ]).then(([routeResponse, driverCarResponse]) => {
                setRoutes(routeResponse.data)
                setDriverCars(driverCarResponse.data)
            }
        )
    }, [selectedOrders])

        const onChange = (time: Dayjs, timeString: string) => {
        setTimer(timeString)
    }
    return (
        <Form
            name="form_create_shipping_slip"
            autoComplete={'off'}
            className={'mt-4'}
            labelCol={{span: 8}}
            wrapperCol={{span: 16}}
        >
            <Modal
                className={'edit-variant'}
                open={isShowModal}
                onCancel={() => {
                    setIsShowModal(false)
                }}
                destroyOnClose={true}
                width={1600}
                footer={[
                    <Button key="back" onClick={() => {
                        setIsShowModal(false)
                    }}>
                        Đóng
                    </Button>,
                    <Button type="primary" htmlType="submit" onClick={handleSubmit(onSubmit)}>
                        Lưu tạo chuyến hàng
                    </Button>
                ]}
            >
                <div>
                    <div className={'uppercase text-primary text-lg font-medium border-b'}>
                        Tạo chuyến hàng
                    </div>
                    <Row className={'mt-4'}>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label={'Tuyến'}
                                       required={true}
                                       validateStatus={errors.onOrder?.routeId && 'error'}
                                       help={errors.onOrder?.routeId && errors?.onOrder?.routeId?.message}
                            >
                                <Controller
                                    name={'onOrder.routeId'}
                                    control={control}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            {...field}
                                            placeholder={'Chọn tuyến'}
                                        >
                                            {routes?.map((route) => (
                                                <Select.Option value={route.id}
                                                               key={`route-${route.id}`}>
                                                    {route.name}
                                                </Select.Option>
                                            ))}
                                        </Select>
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label={'Ngày'}
                                       required={true}>
                                <Controller
                                    name={'onOrder.date'}
                                    control={control}
                                    render={({field}) => (
                                        // @ts-ignore
                                        <DatePicker
                                            {...field}
                                            allowClear
                                            format={'dd/MM/y'}
                                            placeholder={'Ngày'}
                                            className={'w-full'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label={'Chuyến'} required={true}>
                                <Controller
                                    name={'onOrder.time'}
                                    control={control}
                                    render={({field}) => (
                                        // @ts-ignore
                                        <TimePicker
                                            {...field}
                                            allowClear
                                            format={'HH:mm'}
                                            placeholder={'Chuyến'}
                                            className={'w-full'}
                                            onChange={onChange}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item
                                required={true}
                                label={<span>Chọn xe - tài xế </span>}
                                validateStatus={errors.onOrder?.driverCarId && 'error'}
                                help={errors.onOrder?.driverCarId && errors.onOrder?.driverCarId?.message}
                            >
                                <Controller
                                    control={control}
                                    name={'onOrder.driverCarId'}
                                    render={({field}) => (
                                        <Select
                                            filterOption={filterOption}
                                            showSearch
                                            {...field}
                                            placeholder={'Chọn xe - tài xế'}
                                        >
                                            {driverCars?.map((driveCar) => (
                                                <Select.Option value={driveCar.id}
                                                               key={`driverCar-${driveCar.id}`}>
                                                    {driveCar.driverName} - {driveCar.licensePlates}
                                                </Select.Option>
                                            ))}
                                        </Select>
                                    )}
                                />
                            </Form.Item> </Col>
                        <Col xs={24} sm={24} md={24} lg={8}>
                            <Form.Item label="Ghi chú">
                                <Controller
                                    control={control}
                                    name={'onOrder.note'}
                                    render={({field}) => (
                                        <TextArea
                                            {...field}
                                            placeholder="Nhập ghi chú"
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>

                    </Row>
                    <div className={'uppercase text-primary text-lg font-medium border-b mt-8'}>
                        Danh sách đơn gửi hàng
                    </div>
                    <Table
                        dataSource={selectedOrders}
                        scroll={{x: 1000}}
                        size={'small'}
                        {...props}
                        columns={columns}
                    />
                </div>
            </Modal>
        </Form>
    )
}
export default CreateOnOrder