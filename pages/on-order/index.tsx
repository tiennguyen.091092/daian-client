import React, {FC, useEffect, useState} from 'react'
import {useRecoilValue} from 'recoil'
import {countDataTable} from 'recoil/Atoms'
import {useForm, useWatch} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {ColumnsType} from 'antd/es/table'
import {formatDate, formatDateDDMMYYYY} from 'utils'
import Content from 'components/layout/AdminLayout/Content'
import {Card} from 'antd'
import DataTable from 'common/DataTable'
import {OnOrderEntity, OnOrderService} from 'services'

interface Inputs {
    code: string
}

const Index: FC = () => {
    const [title] = useState<string>('Chuyến hàng')
    const countDatatable = useRecoilValue(countDataTable)
    const {control} = useForm<Inputs>({
        defaultValues: {
            code: '',
        },
    })
    const watchCodeFilter = useWatch({
        control,
        name: 'code',
    })
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
        join: [
            'route',
            'driverCar',
            'orders',
            'orders.customerSend',
            'orders.customerReceived',
        ],
    })
    const columns: ColumnsType<OnOrderEntity> = [
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align:'center',
            render: (value) => formatDate(value),
        },
        {
            dataIndex: 'orders',
            title: 'Số đơn hàng',
            align:'center',
            render: (value) => <b>{value?.length}</b> ,
        },
        {
            dataIndex: 'route',
            title: 'Tuyến giao',
            render: (value, record) => record?.route?.name,
        },
        {
            dataIndex: 'driverCar',
            title: 'Xe - Tài xế',
            render: (value, record) => {
                return (<>
                        <b>{record?.driverCar?.driverName}</b>
                        <br/>
                        <span>{record?.driverCar?.licensePlates}</span>
                    </>
                )
            }
        },

        {
            dataIndex: 'date',
            title: 'Ngày',
            align:'center',
            render: (value) => formatDateDDMMYYYY(value),
        },
        {
            dataIndex: 'time',
            title: 'Chuyến',
            align:'center',
        },

    ]
    useEffect(() => {
        if (!watchCodeFilter)
            setTableServiceParams((prevState) => ({
                ...prevState,
                filter: [],
            }))
    }, [watchCodeFilter])
    // const onSubmit: SubmitHandler<Inputs> = (data) => {
    //     // const filter = []
    //     // if (data?.code)
    //     //     filter.push(`code||$cont||${data?.code}`)
    //     //
    //     // setTableServiceParams((prevState) => ({
    //     //     ...prevState,
    //     //     filter: filter,
    //     // }))
    // }
    // const onReset = () => {
    //     setTableServiceParams({
    //         sort: ['createdAt,DESC'],
    //         join: [
    //             'route',
    //             'driverCar',
    //             'orders',
    //             'orders.customerSend',
    //             'orders.customerReceived',
    //         ],
    //     })
    // }
    return (
        <>
            <Content
                title={title}
            >
                {/*<Card>*/}
                {/*    <Form*/}
                {/*        autoComplete={'off'}*/}
                {/*        onFinish={handleSubmit(onSubmit)}*/}
                {/*        onReset={onReset}*/}
                {/*    >*/}
                {/*        <Row gutter={[8, 8]}>*/}
                {/*            <Col xs={24} sm={24} md={24} lg={12}>*/}
                {/*                <Form.Item label={'Mã phiếu gửi hàng'}>*/}
                {/*                    <Controller*/}
                {/*                        control={control}*/}
                {/*                        name={'code'}*/}
                {/*                        render={({field}) => (*/}
                {/*                            <Input*/}
                {/*                                {...field}*/}
                {/*                                placeholder={'Nhập mã phiếu gửi hàng'}*/}
                {/*                            />*/}
                {/*                        )}*/}
                {/*                    />*/}
                {/*                </Form.Item>*/}
                {/*            </Col>*/}
                {/*            <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>*/}
                {/*                <FormItem>*/}
                {/*                    <Space>*/}
                {/*                        <Button htmlType={'reset'}>Đặt lại</Button>*/}
                {/*                        <Button type={'primary'} htmlType={'submit'}>*/}
                {/*                            Tìm kiếm*/}
                {/*                        </Button>*/}
                {/*                    </Space>*/}
                {/*                </FormItem>*/}
                {/*            </Col>*/}
                {/*        </Row>*/}
                {/*    </Form>*/}
                {/*</Card>*/}
                <Card
                    title={<span className={'text-2xl'}>{countDatatable} Chuyến hàng</span>}
                >
                    <DataTable
                        service={OnOrderService.getManyBase}
                        serviceParams={tableServiceParams}
                        columns={columns}
                        action={['print','edit']}
                        path={'/on-order'}
                    />
                </Card>
            </Content>
        </>


    )
}

export default Index