import React, {FC, useEffect, useState} from 'react'
import {countDataTable} from 'recoil/Atoms'
import {useRecoilValue} from 'recoil'
import {Controller, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {formatDate} from 'utils'
import {ColumnsType} from 'antd/es/table'
import {BranchOfficeEntity, BranchOfficeService, LocationEntity, LocationService} from 'services'
import {Button, Card, Col, Form, Input, Row, Space} from 'antd'
import DataTable from 'common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import FormItem from 'common/FormItem'
import Link from 'common/Link'

interface Inputs {
    branchOfficeName: string
}

const Index: FC = () => {
    const [title] = useState<string>('Chi nhánh văn phòng')
    const countDatatable = useRecoilValue(countDataTable)
    const {control, handleSubmit} = useForm<Inputs>({
        defaultValues: {
            branchOfficeName: '',
        },
    })
    const watchbBranchOfficeNameFilter = useWatch({
        control,
        name: 'branchOfficeName',
    })
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        sort: ['createdAt,DESC'],
    })
    const columns: ColumnsType<BranchOfficeEntity> = [
        {
            dataIndex: 'branchOfficeName',
            title: 'Tên chi nhánh',

        },
        {
            dataIndex: 'branchOfficeAddress',
            title: 'Địa chỉ',

        },
        {
            dataIndex: 'branchOfficePhone',
            title: 'SĐT Văn phòng',

        },
        {
            dataIndex: 'representative',
            title: 'Người đại diện',

        },
        {
            dataIndex: 'representativePhone',
            title: 'SĐT',

        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align:'center',
            render: (value) => formatDate(value),

        },
        {
            dataIndex: 'updateAt',
            title: 'Cập nhật lần cuối',
            align:'center',
            render: (value) => formatDate(value),
        },
    ]
    useEffect(() => {
        if (!watchbBranchOfficeNameFilter)
            setTableServiceParams((prevState) => ({
                ...prevState,
                filter: [],
            }))
    }, [watchbBranchOfficeNameFilter])
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        const filter = []
        if (data?.branchOfficeName)
            filter.push(`branchOfficeName||$cont||${data?.branchOfficeName}`)

        setTableServiceParams((prevState) => ({
            ...prevState,
            filter: filter,
        }))
    }
    const onReset = () => {
        setTableServiceParams({
            sort: ['createdAt,DESC'],
        })
    }
    return (
        <Content
            title={title}
        >
            <Card>
                <Form
                    autoComplete={'off'}
                    onFinish={handleSubmit(onSubmit)}
                    onReset={onReset}
                >
                    <Row gutter={[8, 8]}>
                        <Col xs={24} sm={24} md={24} lg={12}>
                            <Form.Item label={'Tên Chi nhánh văn phòng'}>
                                <Controller
                                    control={control}
                                    name={'branchOfficeName'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            placeholder={'Nhập Tên Chi nhánh văn phòng tìm kiếm'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                            <FormItem>
                                <Space>
                                    <Button htmlType={'reset'}>Đặt lại</Button>
                                    <Button type={'primary'} htmlType={'submit'}>
                                        Tìm kiếm
                                    </Button>
                                </Space>
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </Card>
            <Card
                title={<span className={'text-2xl'}>{countDatatable} Chi nhánh Văn phòng</span>}
                extra={
                    <Link href={'/branch-office/create'}>
                        <Button
                            type={'primary'}>
                            Thêm mới
                        </Button>
                    </Link>
                }
            >
                <DataTable
                    service={BranchOfficeService.getManyBase}
                    serviceParams={tableServiceParams}
                    deleteService={BranchOfficeService.deleteOneBase}
                    columns={columns}
                    action={['edit', 'delete']}
                    path={'/branch-office'}
                />
            </Card>
        </Content>
    )
}

export default Index