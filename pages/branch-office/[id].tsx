import {FC, useEffect, useState} from 'react'
import {useRouter} from 'next/router'
import {loadingState} from 'recoil/Atoms'
import {useSetRecoilState} from 'recoil'
import {alertError, modifyEntity} from 'utils'
import {BranchOfficeEntity, BranchOfficeService} from 'services'
import {yupResolver} from '@hookform/resolvers/yup'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import Content from 'components/layout/AdminLayout/Content'
import {Button, Card, Form, Input} from 'antd'

interface Inputs {
    branchOffice: BranchOfficeEntity
}
const schema = yup.object().shape({
    branchOffice: yup.object().shape({
        branchOfficeName: yup.string().required('Chưa nhập Tên chi nhánh'),
    }),
})
const EditBranchOffice: FC = () => {
    const [title, setTitle] = useState<string>('Cập nhật Chi nhánh Văn phòng')
    const setIsLoading = useSetRecoilState(loadingState)
    const router = useRouter()
    const {
        control,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
    })
    const { TextArea } = Input
    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Thêm mới Chi nhánh')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                BranchOfficeService.getOneBase({
                    id: Number(router?.query?.id),
                }),
            ])
                .then(([responseData]) => {
                    setValue('branchOffice', responseData)
                    setIsLoading(false)
                })
                .catch((error) => {
                    alertError(error)
                    setIsLoading(false)
                })
        }
    }, [router])

    const onSubmit: SubmitHandler<Inputs> = (data) => {
        modifyEntity(BranchOfficeService, data.branchOffice, title, () => {
            return router.push(`/branch-office`)
        }).then()
    }

    return (
        <Content title={title}  onBack={() => router.push('/branch-office')}>
            <Card>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    onFinish={handleSubmit(onSubmit)} autoComplete={'off'}
                >
                    <Form.Item
                        label={<span>Tên chi nhánh</span>}
                        validateStatus={errors.branchOffice?.branchOfficeName && 'error'}
                        required={true}
                        help={errors.branchOffice?.branchOfficeName && errors.branchOffice?.branchOfficeName?.message}
                    >
                        <Controller
                            control={control}
                            name={'branchOffice.branchOfficeName'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Tên chi nhánh"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Số điện thoại Chi nhánh</span>}
                    >
                        <Controller
                            control={control}
                            name={'branchOffice.branchOfficePhone'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Số điện thoại"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Tên người đại diện</span>}
                    >
                        <Controller
                            control={control}
                            name={'branchOffice.representative'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Tên người đại diện"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Số điện thoại người đại diện</span>}
                    >
                        <Controller
                            control={control}
                            name={'branchOffice.representativePhone'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Số điện thoại"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Địa chỉ</span>}
                    >
                        <Controller
                            control={control}
                            name={'branchOffice.branchOfficeAddress'}
                            render={({ field }) => (
                                <TextArea
                                    {...field}
                                    placeholder="Địa chỉ"
                                />
                            )}
                        />
                    </Form.Item>

                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Lưu lại
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Content>
    )
}

export default EditBranchOffice