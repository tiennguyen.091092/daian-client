import React, {FC, useEffect, useState} from 'react'
import {useRouter} from 'next/router'
import {loadingState} from 'recoil/Atoms'
import {useSetRecoilState} from 'recoil'
import {alertError, getStatusUser, modifyEntity} from 'utils'
import {EnumUserEntityStatus, UserEntity, UsersService} from 'services'
import {yupResolver} from '@hookform/resolvers/yup'
import {Controller, SubmitHandler, useForm} from 'react-hook-form'
import * as yup from 'yup'
import Content from 'components/layout/AdminLayout/Content'
import {Button, Card, Form, Input, Select} from 'antd'
import {LockOutlined, PhoneOutlined} from '@ant-design/icons'

interface Inputs {
    user: UserEntity
}
const phoneRegExp = /([\+84|84|0]+(3|5|7|8|9|1[2|6|8|9]))+([0-9]{8})\b/
const schema = yup.object().shape({
    user: yup.object().shape({
        tel: yup
            .string()
            .required('Chưa nhập số điện thoại')
            .matches(phoneRegExp, 'Số điện thoại không hợp lệ'),
        password: yup
            .string()
            .required('Chưa nhập mật khẩu.')
            .min(8, 'Mật khẩu quá ngắn - tối thiểu phải có 8 ký tự.'),
        fullName: yup
            .string()
            .required('Chưa nhập tên')
    }),
})
const EditUser: FC = () => {
    const [title, setTitle] = useState<string>('Cập nhật Tài khoản')
    const setIsLoading = useSetRecoilState(loadingState)
    const router = useRouter()
    const {
        control,
        handleSubmit,
        setValue,
        formState: { errors },
    } = useForm<Inputs>({
        mode: 'all',
        resolver: yupResolver(schema),
    })
    useEffect(() => {
        setIsLoading(true)
        if (router.query?.id) {
            if (router.query?.id == 'create') {
                setTitle('Thêm mới Tài khoản')
            }
            Promise.all([
                router.query?.id !== 'create' &&
                UsersService.getOneBase({
                    id: Number(router?.query?.id),
                }),
            ])
                .then(([responseData]) => {
                    setValue('user', responseData)
                    setIsLoading(false)
                })
                .catch((error) => {
                    alertError(error)
                    setIsLoading(false)
                })
        }
    }, [router])

    const onSubmit: SubmitHandler<Inputs> = (data) => {
        modifyEntity(UsersService, data.user, title, () => {
            return router.push(`/user`)
        }).then()
    }

    return (
        <Content title={title}  onBack={() => router.push('/user')}>
            <Card>
                <Form
                    name="basic"
                    labelCol={{ span: 8 }}
                    wrapperCol={{ span: 16 }}
                    style={{ maxWidth: 600 }}
                    onFinish={handleSubmit(onSubmit)} autoComplete={'off'}
                >
                    <Form.Item
                        label={<span>Số điện thoại </span>}
                        validateStatus={errors.user?.tel && 'error'}
                        help={errors.user?.tel && errors.user?.tel?.message}
                        required={true}
                    >
                        <Controller
                            control={control}
                            name={'user.tel'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="large"
                                    placeholder="Số điện thoại"
                                    addonAfter={<PhoneOutlined />}
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Mật khẩu </span>}
                        validateStatus={errors.user?.password && 'error'}
                        help={errors.user?.password && errors.user?.password?.message}
                        required={true}
                    >
                        <Controller
                            control={control}
                            name={'user.password'}
                            render={({ field }) => (
                                <Input.Password
                                    {...field}
                                    size="large"
                                    placeholder="Mật khẩu"
                                    prefix={<LockOutlined />}
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item
                        label={<span>Tên </span>}
                        required={true}
                        validateStatus={errors.user?.fullName && 'error'}
                        help={errors.user?.fullName && errors.user?.fullName?.message}
                    >
                        <Controller
                            control={control}
                            name={'user.fullName'}
                            render={({ field }) => (
                                <Input
                                    {...field}
                                    size="middle"
                                    placeholder="Họ tên"
                                />
                            )}
                        />
                    </Form.Item>
                    <Form.Item label={'Trạng thái'}>
                        <Controller
                            name={'user.status'}
                            control={control}
                            render={({ field }) => (
                                <Select
                                    showSearch
                                    placeholder="Vui lòng chọn trạng thái"
                                    size={'middle'}
                                    {...field}
                                >
                                    <Select.Option value={EnumUserEntityStatus.active}>
                                        {getStatusUser(EnumUserEntityStatus.active)}
                                    </Select.Option>
                                    <Select.Option value={EnumUserEntityStatus.inactive}>
                                        {getStatusUser(EnumUserEntityStatus.inactive)}
                                    </Select.Option>
                                    <Select.Option value={EnumUserEntityStatus.banned}>
                                        {getStatusUser(EnumUserEntityStatus.banned)}
                                    </Select.Option>
                                </Select>
                            )}
                        />
                    </Form.Item>
                    <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
                        <Button type="primary" htmlType="submit">
                            Lưu lại
                        </Button>
                    </Form.Item>
                </Form>
            </Card>
        </Content>
    )
}

export default EditUser