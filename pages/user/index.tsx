import React, {FC, useEffect, useState} from 'react'
import {countDataTable} from 'recoil/Atoms'
import {useRecoilValue} from 'recoil'
import {Controller, SubmitHandler, useForm, useWatch} from 'react-hook-form'
import {ServiceParams} from 'utils/interfaces'
import {formatDate, getStatusUser, getTextTypeCustomer, getTextTypeDriverCar} from 'utils'
import {ColumnsType} from 'antd/es/table'
import {
    CustomerEntity,
    CustomerService,
    EnumCustomerEntityCustomerGender,
    EnumUserEntityStatus,
    UserEntity, UsersService
} from 'services'
import {Button, Card, Col, Form, Input, Row, Space, Tag} from 'antd'
import DataTable from 'common/DataTable'
import Content from 'components/layout/AdminLayout/Content'
import FormItem from 'common/FormItem'
import Link from 'common/Link'

interface Inputs {
    tel: string
}

const Index: FC = () => {
    const [title] = useState<string>('Quản lý Tài khoản')
    const countDatatable = useRecoilValue(countDataTable)
    const {control, handleSubmit} = useForm<Inputs>({
        defaultValues: {
            tel: '',
        },
    })
    const watchTelFilter = useWatch({
        control,
        name: 'tel',
    })
    const [tableServiceParams, setTableServiceParams] = useState<ServiceParams>({
        filter:[''],
        sort: ['createdAt,DESC'],
    })
    const columns: ColumnsType<UserEntity> = [
        {
            dataIndex: 'tel',
            title: 'Số điện thoại đăng nhập',
        },
        {
            dataIndex: 'fullName',
            title: 'Tên',
        },
        {
            dataIndex: 'Email',
            title: 'Email',
        },
        {
            dataIndex: 'createdAt',
            title: 'Ngày tạo',
            align:'center',
            render: (value) => formatDate(value),
        },
        {
            dataIndex: 'updateAt',
            title: 'Cập nhật lần cuối',
            align:'center',
            render: (value) => formatDate(value),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            render: (value) =>
                value === EnumUserEntityStatus.active ? (
                    <Tag color={'success'}>{getStatusUser(value)}</Tag>
                ) : value === EnumUserEntityStatus.inactive ? (
                    <Tag color={'warning'}>{getStatusUser(value)}</Tag>
                ) : value === EnumUserEntityStatus.banned ? (
                    <Tag color={'error'}>{getStatusUser(value)}</Tag>
                ) : null,
        },
    ]
    useEffect(() => {
        if (!watchTelFilter && watchTelFilter!='')
            setTableServiceParams((prevState) => ({
                ...prevState,
                filter: [],
            }))
    }, [watchTelFilter])
    const onSubmit: SubmitHandler<Inputs> = (data) => {
        const filter = []
        if (data?.tel){
            filter.push(`tel||$cont||${data?.tel}`)
        }
        setTableServiceParams((prevState) => ({
            ...prevState,
            filter: filter,
        }))
    }
    const onReset = () => {
        setTableServiceParams({
            sort: ['createdAt,DESC'],
        })
    }
    return (
        <Content
            title={title}
        >
            <Card>
                <Form
                    autoComplete={'off'}
                    onFinish={handleSubmit(onSubmit)}
                    onReset={onReset}
                >
                    <Row gutter={[8, 8]}>
                        <Col xs={24} sm={24} md={24} lg={12}>
                            <Form.Item label={'Số điện thoại'}>
                                <Controller
                                    control={control}
                                    name={'tel'}
                                    render={({field}) => (
                                        <Input
                                            {...field}
                                            placeholder={'Nhập số điện thoại tìm kiếm'}
                                        />
                                    )}
                                />
                            </Form.Item>
                        </Col>
                        <Col xs={24} sm={24} md={24} lg={12} className={'text-right'}>
                            <FormItem>
                                <Space>
                                    <Button htmlType={'reset'}>Đặt lại</Button>
                                    <Button type={'primary'} htmlType={'submit'}>
                                        Tìm kiếm
                                    </Button>
                                </Space>
                            </FormItem>
                        </Col>
                    </Row>
                </Form>
            </Card>
            <Card
                title={<span className={'text-2xl'}>{countDatatable} Tài khoản</span>}
                extra={
                    <Link href={'/user/create'}>
                        <Button
                            type={'primary'}>
                            Thêm mới
                        </Button>
                    </Link>
                }
            >
                <DataTable
                    service={UsersService.getManyBase}
                    serviceParams={tableServiceParams}
                    deleteService={UsersService.deleteOneBase}
                    columns={columns}
                    action={['edit']}
                    path={'/user'}
                />
            </Card>
        </Content>
    )
}

export default Index